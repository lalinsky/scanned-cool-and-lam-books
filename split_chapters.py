#!/usr/bin/env python

import sys

out = None
chapter = 1

for line in open(sys.argv[1]):
    line = line.strip()
    if line.lower().startswith('chapter '):
        out = open('chapter%02d.txt' % chapter, 'w')
        chapter += 1
    elif not out:
        out = open('chapter00.txt', 'w')
    print >>out, line

