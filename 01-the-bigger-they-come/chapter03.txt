## Chapter Three

We sat in a little, quiet restaurant down on a side street, a
little place run by a big German woman. It was a new one on me.
Alma Hunter said Sandra had been eating there for five or six
months. The food was wonderful.

"Tell me—how long have you been working there?" Alma asked.

"You mean at the detective agency?"

"Yes, of course."

I said, "About three hours."

"I thought so. And you've been out of work for a while?"

"Yes."

"How in the world did a man of your size decide to become—er—that
is—what experience have you had—or perhaps I shouldn't ask that."

"You shouldn't," I said.

She was silent for a moment, then said, "I'm going to give you some
money for the lunch check. We'll do that on all meals we eat
together. I don't want to put you in the position of standing by
while I pay the check. As a man, you'll naturally resent—"

"Don't worry about me," I grinned. "All the pride I ever had has
been kicked out of me. You've seen that for yourself."

"You mustn't be like that," she protested. Her eyes showed hurt.

"Ever walked the streets," I asked, "hungry—not able to talk to
anyone, because the people you know would cut you and the people
you didn't know would think you wanted a handout? Have you ever
been counted out without having had any sort of a trial?"

"No," she said, "I don't think I have."

"Try it some time," I said. "It does a lot for your pride."

"You mustn't let it get you down."

"Oh no, it hasn't," I assured her politely.

"Now you're being sarcastic," she said. "I don't think, Mr.—I'm
going to call you Donald. You call me Alma. When people are mixed
up in a game such as we're mixed up in, it seems foolish to stand
on a lot of formality."

"Tell me about the game we're mixed up in," I invited. There was a
queer expression in her eyes, a pleading perhaps, perhaps a
loneliness, and, I thought, just a glint of fear.

"Tell me, Donald, and tell me the truth. You haven't had any
previous experience as a detective, have you?"

I squeezed the last drops of coffee out of the coffee pot, and
said, "Lovely weather we're having, isn't it?"

"That's what I thought."

"What is?"

She smiled. "That we're having lovely weather."

"That makes it unanimous," I observed.

"I didn't mean to hurt your feelings, Donald."

"You haven't. My feelings don't get hurt."

She leaned forward across the table. "I want you to help me,
Donald."

"You heard what Mrs. Cool told you," I said, "that you could put me
cm a collar and lead me around on a leash if you wanted to."

"Oh, Donald, please don't be like that. I understand how you must
have felt. But don't take it out on me."

"I'm not. I'm trying to tell you that this is a business
arrangement."

"I want it to be personal as well—you're hired to serve papers on
Morgan Birks, but there are a lot of things about the case you
should understand, and—and I want you to help me a little bit."

"Go on," I said, "it's your party."

She said, "Morgan was mixed up in this slot-machine business right
up to his ears. It's a sordid story. There's graft, bribery, and
corruption. Those machines were all adjusted so there was a
terrific pay-off. They had to be. Morgan had to take care of the
police. The places that leased them had to be given a big profit."

"Nothing particularly unusual about that, is there?"

"I don't know," she said. "It's my first experience with anything
of the kind. I was shocked—and Sandra's changed a lot."

"Since when?"

"Since two years ago."

"Is that when she was married? In other words, since her
marriage?"

"Yes."

"Did you know Morgan Birks before they were married?"

"No. I've never met him. He didn't like me."

"Why?"

"I think Sandra used me as a scapegoat. She wrote me long letters
after they were married. You see, she was married on her vacation.
She'd been saving her money for three years to make a trip to
Honolulu. She met Morgan on the boat. They were married in
Honolulu. She sent a wire giving up her job."

"What were you a scapegoat for?"

"Oh, lots of things," she evaded.

"Such as what? What's wrong with the way she acts?" "Oh, around
men. I guess Morgan's old-fashioned in his ideas, and I guess he's
horribly jealous. He tells Sandra she's nothing but an
exhibitionist."

"Is she?"

"No, of course not. Sandra's frank and modem and— well, she doesn't
have any old-fashioned modesty about her body."

"Didn't Morgan Birks know that before he married her?" She smiled
and said, "Men like women to be modem with them. It's when they're
modem with other men that the trouble starts."

"And Sandra blamed you?" I asked.

"No. But I think Morgan did. He thought someone had undermined
Sandra's feeling about—well, about things like that, and because
she'd been rooming with me, Morgan thinks I'm responsible."

"And how has Sandra changed?"

"I don't know. She's grown sort of hard and watchful and shrewd and
calculating. She looks at you, and you get the feeling that she's
hiding behind her eyes."

"When did you notice this?"

"As soon as I saw her again."

"And when was that?"

"About a week ago when this thing broke. She wrote and asked me to
come and stay with her for a while."

"You're working?" I asked.

"No—not now. I burnt my bridges. I gave up my job to come and live
with Sandra for a while."

"Do you think that was wise?"

"She told me I could get another job here."

"Where had you been working?"

"Kansas City."

"And that was where you met Sandra—where you roomed together?"

"No. Sandra and I were rooming together in Salt Lake City. She met
Morgan on that Honolulu trip and never even came back for her
things. I sent them on to her at Kansas City. Then, after a while,
Morgan left and came here, and I drifted back East and had a job in
Kansas City, but I wasn't there while Morgan was and—at least I
don't think I was. I didn't keep up with Sandra. Morgan, you see,
goes to places, stays there for a while, and then gets kicked out.
Things get hot for him—well, like they are here, only this is the
worst it's ever been."

The big German woman came to beam down at us and ask if we wanted
any more coffee. Alma said, "No."

I said, "Yes"

She took my coffee pot away to fill it, and I said to Alma, "I'm
doing about as much talking as you are. If you want to tell me
things, why not go ahead and tell them?"

"What," she asked, ‘'did you want to know?”

"Everything."

"I used to be simply crazy about Sandra," Alma said. "I guess I
still am, but marriage has changed her a lot, all right, that and
the sort of life she's lived with Morgan Birks." She laughed
nervously and said, "I guess you think it's a scream. Morgan
blaming me for the things he doesn't like in Sandra, and I claiming
that Morgan has been responsible for a change in Sandra. I—"

"For God's sake," I said, "tell me the truth. What's the nutter
with Sandra? Is she on the make?"

"You couldn't blame her if she was," Alma said hotly. "Morgan never
has been true to her. Within the first few months after her
marriage, she found out he was keeping a mistress. It's been like
that ever since."

"The same girl?" I asked.

"No. He couldn't even be true to a mistress."

"Well, according to your ideas," I said, "that's because Sandra
didn't make a home for him, didn't—"

"Donald," she interrupted, "don't be like that. Now stop it."

The German woman brought my coffee. I said, "All right. I'm
stopped, but you do think it's about six of one and a half a dozen
of the other, don't you?"

"Morgan threw Sandra in with a wild crowd," she said. "He
associates with gamblers and men of that type, and occasionally
there'd be some politician he'd want Sandra to play up to. He'd
keep telling her, 'My God, don't be so stiff. Go ahead and use a
little sex appeal on this guy. I want him to like you. He's
important to us.' He was after Sandra all the time to be sort of a
glamour girl."

"All right," I said, "she's your friend. You won't say anything
against her. There's no use wasting time trying to argue about it.
Now, go ahead and tell me the rest of it"

"The rest of what?"

‘‘The rest of what's worrying you.”

"I think she's got some money that belongs to Morgan Birks."

"Where did she get it?"

"It's pay-off money. I think there were some safety deposit boxes
in her name, or perhaps she'd an assumed name for those boxes.
Morgan gave her money to put in there— I think it was money he had
been given for bribery or something. I don't know. But anyhow,
Sandra doesn't intend to let him get that money back."

"I take it," I said, "that when she plays marbles, she plays for
keeps."

"Well, can you blame her?" Alma Hunter asked.

"I don't know," I said, "yet."

"Well, what I'm trying to tell you is that I'm afraid."

"Of what?"

"Of everything."

"Of Morgan Birks?"

"Yes."

"And is Sandra afraid of him?"

"No, and that's what bothers me. I think she should be."

"Did you read the divorce complaint?"

"Yes."

"Did you notice that she was trying to grab everything in sight?
She wants to cash in on the life insurance, have a receiver
appointed for the property, collect temporary alimony and
attorney's fees, have a share of the community property awarded to
her, and an allowance by way of alimony."

"That's what the lawyer put in. Lawyers always do that."

"Is that what Sandra told you?"

"Yes."

"And what do you want me to do?"

"You're right about Sandra; when she starts fighting, she fights,"
she said. "She was always that way. One night a boy friend wasn't
going to go home. He got rough, and Sandra was going to hit him
with one of her golf clubs. She'd have done it, too."

"What stopped her?"

"I did."

"What happened to the boy friend?"

"He was frightened. I talked him into going home. He wasn't a
friend, just an acquaintance."

"All right. Go on."

"Well, Sandra acts as though she were keeping something back from
me, and I'm afraid she is. I think she's trying to take some
advantage of Morgan. I don't know just how or what, and—well, I
want you to find out and see what you can do to make her—well, be
reasonable."

"And that's all?" I asked.

"Yes."

"How about you? Wasn't there something you wanted?"

She looked at me appraisingly for a moment, then slowly shook her
head and said, "No."

I finished my coffee. "Go right ahead," I said. "Keep on thinking
that I'm a babe in the woods who shouldn't be trusted out alone
after dark. You know damn well that if I'd told you I had two or
three years as a detective back of me, you'd have told me what's
really on your mind. The way it is now, you figure I can't be
trusted."

She started to say something then, but checked herself just as she
started to speak.

"Go ahead," I said. "Pay the check, and let's go meet the brother,
and see what he has to say."

"And you won't tell anyone what I've told you?"

"You haven't told me anything—what did you say the brother's name
was?"

"Thoms."

"His first name?"

"I don't think I've ever heard it. It's B. Lee Thoms. That's the
way he signs his name. Sandra calls him Bleatie. She always has."

I motioned to the German woman to bring us the check, and said,
"Let's go see Bleatie."



