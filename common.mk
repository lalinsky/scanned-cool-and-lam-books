include metadata.mk

file = aa-fair-$(shell basename $(CURDIR))

all: epub mobi

epub: html
	ebook-convert $(file).html $(file).epub \
		--extra-css ../stylesheet.css \
		--cover cover.jpg \
		--title "$(title)" \
		--authors "$(author)" \
		--publisher "$(publisher)" \
		--pubdate "$(year)" \
		--series "$(series)" \
		--series-index "$(series_index)" \
		--language=en-US

mobi: html
	ebook-convert $(file).html $(file).mobi \
		--extra-css ../stylesheet.css \
		--cover cover.jpg \
		--title "$(title)" \
		--authors "$(author)" \
		--publisher "$(publisher)" \
		--pubdate "$(year)" \
		--series "$(series)" \
		--series-index "$(series_index)" \
		--language=en-US

epub-dir: epub
	rm -rf $(file)
	mkdir $(file)
	unzip $(file).epub -d $(file)

html: title.txt intro.txt chapter*.txt
	pandoc --section-divs -S -s -t html -o - $+ \
		| xmlstarlet fo -e utf8 - \
		| xmlstarlet ed -N 'x=http://www.w3.org/1999/xhtml' \
			-a '//x:div[position()>1]/x:p[1]' \
			-t attr -n 'class' -v 'first' \
		| xmlstarlet ed -N 'x=http://www.w3.org/1999/xhtml' \
			-d '//x:div[@id="header"]' \
		| xmlstarlet ed -N 'x=http://www.w3.org/1999/xhtml' \
			-d '//x:h1[@class="title"]' \
		> $(file).html

title.txt: metadata.mk
	echo "% $(title)" >$@
	echo "% $(author)" >>$@
	echo "% $(year)" >>$@

clean:
	rm -rf title.txt $(file).*

