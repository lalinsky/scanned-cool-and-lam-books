## Chapter Four

I CLIMBED THE STAIRS to Helen Framley's apartment once more. My
face was sore now. The tips of my fingers showed there was a bump
on the right side of my jaw, and another just below my left
cheekbone. I didn't think they'd show badly, but they hurt.

I rang the bell and waited.

There was no answer.

I rang again.

Abruptly the door of the adjoining apartment opened. The woman who
had talked with me before, said, "Oh, it's you. I think she's in
now. I thought you were ringing this apartment. What's the matter?
Won't she answer?"

I said, "Give her time. She may not have heard the bell."

"Humph! I can hear it in my apartment as plain as my own bell. I
certainly thought you were ringing my bell. Perhaps—"

The man's voice called impatiently from the back of the apartment,
"Maw, get away from that door and quit prying into other people's
business."

"I don't pry into other people's business."

"No, not much."

"I thought he rang my bell, and—"

"Get away from that door."

The door slammed.

I rang Helen Framley's bell again.

Her door opened a cautious inch. I could see the brass chain which
kept it from opening wider, could see cool, slate-colored eyes
looking at me, and then heard her exclamation of surprise. It was
the slot-machine girl. "How did you find me?"

-

"May I come in?"

"No— Certainly not— What do you want?"

"It doesn't have anything to do with what happened at the Cactus
Patch—and it's important."

She hesitated a moment as though turning something over in her
mind, then slid the guard chain back out of its catch.

She studied me curiously as 1 walked on in.

"Don't pay any attention to the face," I said. "It'll come back
into shape after a while."

"Did he hit you hard?"

"I guess it was hard. I felt like a flock of tenpins when a bowler
makes a strike. I've often heard them explode all over the alley,
and now I know just how they must feel."

She laughed, said, "Come on in here and sit down."

I followed her into a little living-room. She indicated a chair. I
sat down.

"Weren't you sitting here?" I asked.

"No. I was sitting over here."

The chair was warm.

"Mind if I smoke?"

"I should say not. I was smoking when you came in." She picked up a
cigarette from an ash tray which was by her chair.

I said, "I'm going to put the cards on the table."

"I like people who do that."

"I'm a private detective."

Her face became cold and white, frozen into a stiffly starched look
of courteous attention.

"What's wrong with that?" I asked.

"N-n-nothing."

"Don't you like private detectives?"

"It depends on—on what they're after."

"I'm after information about a friend."

"I—I'm afraid I can't help you. I—"

I heard a hinge squeak. She flashed a quick glance past me, then
shifted her eyes and kept silent, waiting for something.

I said, without turning my head, "You might just as well come on
over and join the party, Sid."

I heard quick motion behind me, sensed someone standing close to
the back of my chair. "Get all your cards on the table, brother," a
man's voice said.

"All of them that concern you are on the table."

I turned around and looked at him then. It was the man in the plaid
sport coat who had been playing the quarter machine, and I noticed
now he had just the' trace of a cauliflower on his right ear. He
was uneasy—and dangerous.

"Sit down," I told him, "and join the party. I'm not holding
anything back."

He said, "You happened to stumble into the Cactus Patch at a queer
time tonight. Maybe it was just luck—and then—"

I said, "Don't talk so loud. The woman in the next, apartment is
curious."

"I'll say she is," Miss Framley said.

The man in the plaid coat sat down. He said, "We're not going to
say anything for about five minutes. During those five minutes,
you're going to do a lot of talking."

"There'll be just about four minutes of silence then," I said. "My
name's Donald Lam. I'm working for the B.

Cool Detective Agency. I'm trying to locate Corla Burke.

I have reason to believe Miss Framley knows where she is." His face
twitched. "What you want to locate her for?"

"A client."

"Ain't you smart?"

"I'm not trying to be, but I'm not dumb enough to go around telling
the names of our clients to anyone who happens to ask."

He said, "Well, Miss Framley doesn't have any idea where Corla
Burke is 'cause she don't know any Corla Burke."

"Why did Miss Framley send her a letter then?"

"She didn't."

"I know people who say she did—people who are in a position to
know."

"Well, they're cockeyed. She didn't send any letter." Miss Framley
said, "I don't even know who Corla Burke is. You're the second
person who's asked me."

Sid flashed her a quick glance. "Who was the first one?" - "An
engineer out at the dam."

His eyes glittered. "Why didn't you tell me about him?"

"Why should I? I didn't know what he was talking about even. He got
the wrong number somewhere." She turned 'to me and said, "And I
presume he's the one who tipped you off, and that's why you're
here."

"What was this man's name?" I asked.

"The one who asked me the first time?"

"Yes."

She started to answer, then glanced at Sid Jannix, and hesitated
perceptibly.

"Go on," he said.

"I don't know his name. He didn't give it to me."

"You're lying."

She flared up. "Why should I lie to you, you big baboon? My God, do
you want to know every agent that comes to the door trying to sell
a new vacuum sweeper?"

He turned to me and said, "What gave you the idea she'd written a
letter?"

"Some people thought she had."

"Who were they?"

"People who reported to the agency. The agency sent me out."

"Who were the people?"

"You'd have to ask the agency."

He said to Helen Framley, "But you didn't write any letter?"

"No, of course not."

He turned back to me. "What was that you called me—what name?"

"I don't get you."

"When I first came out, you said something—"

"Oh, I called you Sid."

"Where'd you get that name?"

"Isn't that your name?"

"No."

"Pardon me, my mistake. What is it?"

"Harry Beegan."

"Sorry."

"Who told you to call me Sid?"

"I thought, that was your name."

He scowled at me, said slowly, "Get this straight. My name's Harry
Beegan. My nickname is Pug. I don't want to be called by any other
name."

"Okay, that's fine by me."

He turned back to Helen Framley. There were lights in his eyes,
little lights coming and going, like the reflection of sky in a
mountain pool when the wind blows it into little ripples. "If I
thought you was two-timin' me," he said, "I'd—"

"Get it out of your head once and for all," she said, "that you can
frighten me, and I'm not your slave. I'm living my own life. Ours
is a business partnership, and that's all."

"Oh, yeah?"

"You heard me."

He swung back to me. "I want to know some more about this client of
yours."

"You can ask Bertha Cool about that. She's at the Sal Sagev
Hotel."

"That client here in town right now?"

"You'll have to ask her about that."

"I think," he said, "I'm going to take quite an interest in that
client of yours."

"I wouldn't," I told him, "not after what Kleinsmidt told me about
you."

"Who's Kleinsmidt?"

"The big cop who collared me at the blowoff."

"How did you happen to horn in on that?"

"I didn't. I walked in and won a jackpot."

He said, "You weren't dumb enough to tap a nickel machine' when the
ten-cent and two-bit machines were all ripe, were you?"

I said, "I had nickels so I played nickels."

I saw that he was studying me with a puzzled look on his face.

"Did you take out a phoney rivet and leave it out?"

I said, "I don't know about any phoney rivets. I put in nickels and
didn't win anything until a couple of cherries showed up. The next
time I hit the jackpot right on the nose."

"Then what?"

"Then the attendant moved over, and we started arguing."

"Go on."

"Then the manager showed up, and the law. The law was named
Lieutenant William Kleinsmidt. They took me up to the office and
turned me inside out."

"Find anything?"

"A bunch of nickels and—"

"You know what I mean. Piano wire, drill, cups, or any of that
stuff?"

The girl said, "Pug, I believe he's on the outside."

"Don't be too sure," Pug said without taking his eyes off of me.
"What did they find?"

"They found," I said, "that I'd hit Las Vegas a couple of hours ago
on the plane. They found that I hadn't been here before for six
months, that I'm a private detective, that I'm employed by Bertha
Cool, and that Bertha Cool was in the Sal Sagev Hotel waiting for
me to make a re port."

Pug looked me over carefully. "Wouldn't it be a scream," he said,
"if you were telling the truth?" I said, "Kleinsmidt thought I was
telling the truth."

"He's dumb."

"And Breckenridge, the manager, thought I was telling the truth."

"Do you mean to say you just blundered in there and didn't know the
machines were fixed?"

"The woman next door told me I could find Helen Framley hanging out
around the slot machines at the Cactus Patch."

They exchanged glances. Pug gave a low whistle. "How did she know?"
the girl asked.

"She said she'd seen you there several times as she walked past."

"I wish she'd mind her own business for a change," the girl said
savagely. "She told you about Pug being in here, too, just now,
didn't she?"

I nodded, then said, "She didn't have to. I knew he was in the
closet."

"Yes, you did," Pug said derisively.

I said, "The chair was warm. The girl was smoking a cigarette. Her
cigarette was in the ash tray over by that other chair. She leaves
lipstick on the paper. The cigarette, in this ash tray didn't have
any lipstick."

Pug said, "By God, he is a detective."

"Do I get what I want about Corla Burke?"

"We haven't anything, Honest Injun," the girl said. "You don't know
anything about her?"

"No, honest—except what I read in the newspapers."

"You read what the newspapers had to say?"

"Yes."

"Las Vegas newspapers?"

She glanced at Pug, then let her eyes slide away from his. Pug said
to me, "Forget it. You ain't goin' to cross examine her."

"I can ask her questions, can't I?"

"No."

I said, "I don't think there was anything published in the Las
Vegas newspapers. The Los Angeles papers didn't give it a big play.
The man she was to marry wasn't prominent enough to make it a
subject of general interest. It was just another disappearance."

"Well, she says she doesn't know anything about it."

"Except what she read in the papers," I pointed out. Pug's scowl
creased his forehead. "Listen, guy, you've gone far enough, see?"

I said, "I don't see."

"Well, maybe something will happen to improve your - eyesight."

I said, "It costs money to get me working."

"What's that got to do with it?"

"It means that the people who have hired my agency to find Corla
Burke are willing to spend money."

"Okay, let 'ern spend it."

"And," I said, "if a Los Angeles grand jury got the idea there was
something back of that disappearance, it would call witnesses."

"That's fine. Let 'em go ahead."

"The witness who testified before the grand jury would be
testifying under oath. Any lies they told would be per j ury, and
you know what that means. Now, I'm here as a friend. You can tell
me whatever it is you know, and I'll try to find Corla Burke. I
could leave you out of it—if I got results. If you appear before
the Los Angeles grand jury, the situation might be different."

"Forget it. I don't want to appear before no grand jury." I lit a
cigarette.

Helen Framley said, "Well, I'll tell you. I—"

"Skip it," Pug said.

"Shut up, Pug. I know what I'm doing. Let me tell it."

“You're talking too much.'

"No, I'm not. I'm not talking enough. Now, listen, Mr. Lam, I'm
just like any other woman. I'm curious. Well, after this Mr.
Dearbor—this engineer started asking me questions, I made up my
mind I'd find out what he was talking about, so I wrote to a friend
in Los Angeles to get clippings from the newspapers."

"Now," I said, "we're doing a lot better. How about the
clippings?"

"They were mailed to me."

"What did you learn?"

"Nothing you don't know. Just the stuff that was in the
newspapers."

"I haven't seen the papers," I said. "I was only employed on the
case a short time ago. You got those clippings with you?"

"They're in the bureau drawer."

"How about letting me see them?"

"Lay off," Pug said.

"Oh, forget it, Pug. There's no reason why he shouldn't see the
newspaper clippings."

She jumped to her feet, eluded Pug's reaching hand with a swiftly
graceful motion, vanished into the bedroom, returned after a moment
with some newspaper clippings. I glanced through them. They had
been cut from a newspaper and fastened together with a paper clip.
The line along the edge of the paper was in irregular waves as
though the cutting had been done very hastily.

"Could I take these for a few hours?" I asked. "I'd bring them back
in the morning."

"No," Pug said.

I handed them back to her.

"I don't see why not, Pug," she said.

"Listen, babe, we ain't going to help the law in this thing. If
that girl took a powder, she had her own reason for doing it. Let's
mind our own business and keep ourown noses clean."

Pug turned to me. "I don't exactly get you," he said. "What about
me?"

"That slot machine. There was something funny about it. You don't
work that racket?"

I shook my head.

"Not even as a side line?"

I said, "Listen, when it comes to slot -machines, I'm a babe in the
woods. There's one in the Golden Motto—the restaurant where I eat
in Los Angeles. It isn't supposed to be there, but it's in one of
the private dining-rooms, and the regular customers know about it.
Bertha Cool goes crazy at the way I throw money away on that
machine. Every time I go in, I look through my pockets for nickels.
Ordinarily, I only play fifteen or twenty cents. I don't think I've
ever won anything out of the machine except a couple of small
pay-offs."

He said, "Serves you right. Machines that are in restaurants that
way are after a quick take. They don't go for steady customers.
They put rollers on the sprockets so winning two cherries and a
bell is darn near as hard as winning the jackpot or the gold medal
award."

I said, "Other people seem to win on it two or three times a week.
The woman who runs the place will tell me about some of the
salesmen who are pretty lucky on it."

"They're supposed to win?"

"They've won the jackpot three or four times."

"You never saw 'em do it?"

"No. That's what the woman who runs the restaurant says. She tells
me about 'em every so often."

He gave a contemptuous snort and said, "That's kindergarten stuff.
She's probably telling the salesmen about how there's a private
detective who keeps the machine milked dry by playing twenty-five
cents to half a dollar and always coming out a heavy winner."

Helen Framley said to me, "You certainly have nerve."

"Why?"

"Standing up to Pug the way you do. Most people are afraid of him.
I guess that gets your goat, doesn't it, Pug?"

"What?"

"To have this man so independent?"

"Aw, nuts."

"I didn't mean anything, Pug."

"Well, see that you don't."

She turned slate-gray eyes on me again. "You must get around a lot.
You know, get to know different types."

"Not much."

"What are you going to do with Corla when you find her?"

"Talk with her."

"Then are you going to tell -the man who was going to marry her?"

I grinned and said, "I'll tell my boss. She'll tell our client. Our
client will use the information any way he damn pleases. I don't
care what he does with it. He pays Bertha Cool, and Bertha Cool
pays me money. That's all there is to it."

Pug said, "It's like I tell you, babe. Everybody in this world is
on the make. You've got to take it where you can find it."

She grinned across at me. "Pug thinks I'm developing a
conscience."

"On the slot-machine racket?"

"Uh huh."

Pug said, "Forget it, babe."

She said, "The machines are all dishonest. They're stealing from
the customer. Why shouldn't we lift some from the machine?"

"It ain't stealing," Pug said. "It's just taking back some of the
public's investment—and we're the public, ain't we? As far as the
slot machine is concerned, we are. They use mechanical devices to
keep the machines from paying off,and we use mechanical devices to
make 'em pay off. It's fifty-fifty."

I said, "I think this man, Kleinsmidt, is going to be laying for
you. He—"

"Oh, sure," Pug said. "We've got to blow. They always told me never
to try working Nevada with all the protection they've got here, but
I had to have a crack at it. California's different. Take Calermo
Hot Springs for instance. You can always get a good play there.
That's the worst of it. Good play means competition. I remember one
time we tried to work a resort right after another gang had pulled
out. The owners had been checking up on the machines, and when they
found how small the take was, they had some private detectives come
down to see what was happening, and who was doing it."

Helen Framley laughed nervously, and said, "That's where I got my
complex on private detective& They almost nailed us."

"It wouldn't have done 'em any good," Pug said. "They might have
made a lot of trouble."

"They could have talked," Pug admitted, "but that's all."

"Well, I don't like it, Pug. I wish you'd get something else lined
up."

"This is plenty good, babe, plenty good."

I said casually, "I'm going to have to get back to Los Angeles."

Pug said, "You're acting awfully funny about this thing. You
wouldn't by trying to hand us no line, would you?" I shook my
head.

Pug frowned and stared at me with his eyes sharp with suspicion.
Abruptly, he said, "Get your things together, babe."

"What do you mean?"

Pug's eyes grew hostile. "There's just a chance this guy's trying
to stall us along until the law can get us spotted.

Where you got those coins?"

"In my—you know."

"Okay," Pug said, "beat it out and get 'em changed. If they raid
the joint, we don't want to have a lot of dimes and nickels and
quarters on hand. And you, buddy, you better be going. Like you
said, you've got a lot of things to do."

"I'd like to ask a few more questions."

Pug got to his feet, came over, and put his hand on my shoulder. "I
know you would, but we're busy. We've got things to do. You know
how it is."

"Now, Pug, don't you hurt—"

"Forget it, babe. Get that stuff together and get it changed into
currency. This guy's leaving right now, and you've got work to
do."

Her eyes studied Pug for a minute, then came over to mine. Abruptly
she smiled, walked over, and gave me-her hand. "You're one swell
guy," she said. "I like guys with nerve. You sure have plenty."

"Go on. Get in that bedroom and get that stuff together," Pug said
sharply.

"On my way."

Pug started me toward the door. " 'By," I said to Helen Framley,
"and thanks. Where can I reach you if I want to get in touch with
your It was Pug who answered the question, and his eyes were
cold."That, buddy, is the thing I was going to tell you when I got
you outside, but I might as well tell you now. You can't."

"Can't what?"

"Can't get in touch with her."

"Why not?"

"For two reasons. One of 'em is that you won't know where she is,
and the other one is I don't want you to. Get me?"

Helen said, "Pug, don't be like that."

Pug said, "On your way," and gripped his fingers around my elbow.
The push which he exerted was gentle but insistent. Over his
shoulder, he said, "You get into that bedroom, babe, and make it
snappy."

Pug opened the door. "So long, guy," he said. "Nice meeting you.
Don't come back. Good-by."

The door slammed.

I looked at the door of the adjoining apartment and saw that there
was a ribbon of light coming out from under the door.

I tiptoed gently down the stairs.

I walked out and stood in a doorway, watching the sidewalk, and
waiting. The street lights were on now.

After a while, I saw Helen Framley walking down the street, a neat
little package that would attract attention anywhere.

I sauntered along behind.

She went into one of the casinos, and started playing the wheel of
fortune long enough -to register with the gang around the place as
one of the players. Then she went over to the cashier's desk,
opened her purse, pulled out an assortment of nickels, dimes, and
quarters and got them changed into currency. She came out, crossed
the street, ' went to another casino, and repeated the operation.
When she came out of that place, I was waiting for her.

"Hello," I said.

There was sudden fear in her eyes. "What are you doing here?"

"Standing here."

"Well, you mustn't be seen talking with me."

"Why not? I have a couple of questions I wanted to ask you
privately."

"No, no, please. You can't."

"Why not?"

She looked around her apprehensively. "Can't you understand? Pug's
jealous. I had an awful time with him after you left. He thinks
I—thinks I was too nice to you, that I was trying to protect you."

I fell into step at her side. "That's all right. We'll walk along
the street and—"

"No, no," she said, "not this way. Here, walk the other way if
you've got to walk. Turn to the right at the corner. Get down that
dark side street. Gosh, I wish you wouldn't take chances like
this."

I said, "You wrote a letter to Corla Burke. Why and what did you
say?"

"Why, I never wrote her in my life."

"You're certain?"

"Yes."

"You didn't send her a letter a couple of days before she
disappeared?"

"No."

I said, "She was blond. I don't think, she was the type to do
things on impulse exactly. Like to see her picture?"

"Gosh, yes. You got one?"

I guided her into a lighted doorway and took the pictures from my
pocket. They were a little cracked where Louie had pushed wrinkles
in my coat when he jerked it back from my shoulders and down my
arms.

"See. She looks quick on the trigger, but she's a thinker:"

"How can you tell that?"

"From the lines of her face."

She said, "Gosh, I wish I knew things like that."

"You do. You unconsciously size up a person's character as soon as
you meet him. Perhaps you know someone with thin nostrils and—"

"Yeah, but I size 'em up wrong about half the time. Gosh, the
double crosses I've had handed me, just because I play wide open. I
take a good look at 'em and either like 'em or don't. If I like
'em, I go the whole hog. Say, listen—Your name's Donald, isn't
it?"

“Yes.'

"Okay. Now listen, Donald, we've got to cut this out. Pug's awfully
mean when he gets jealous, and he certainly is on the prod tonight.
The way he was feeling when I left, he's almost certain to get
restless and start following us. That's the trouble with Pug. He
won't stay put. When he gets nervous, he gets all excited."

"Where can I get in touch with you, Helen?"

"You can't."

"Isn't there some way I could reach you, some friend to whom I
could write—"

She was shaking her head emphatically.

I gave her one of my cards. "There's my address," I said. "Will you
think it over and see if you can't figure out some way I could keep
in touch with you? Some place I can get you in case it should be
important, to have your testimony?"

"I don't want to give any testimony. I don't want to be dragged
into the limelight and have a lot of questions asked me."

"You can trust me. If you shoot square with me, I'll play square
with you."

She slipped my card in her purse. "I'll think it over, Donald.
Perhaps I can drop you a card, letting you know where you can get
in touch with me." -

"Do that little thing, will you?"

"Perhaps—Donald, can I tell you something—and have you play ball?"

"What?"

"I wasn't telling you all the truth up there."

"I was afraid of that."

"Listen, I want to go some place where we can talk, and Pug may be
down any minute."

"The hotel lobby or—"

"No, no, some place right close. Here, come over in this—Now
listen, Donald, I want to know exactly why you thought I was
holding out."

I said, "I just thought so. And 1 have evidence that you sent a
letter to Cora Burke."

"I haven't lied to you. I just haven't told you all the truth. I'm
going to give you a break. I wanted to up there, but I couldn't on
account of Pug. I didn't know what to do. I finally decided that if
you had the nerve to be waiting for me when I came out, I'd tell
you—maybe."

"What is it?"

"She did write to me."

"That's better. When?"

"The day before she disappeared, I guess it must have been."

"And you'd written to her?"

"No, I hadn't. Honest and truly. I'd never seen her in my life. I
didn't know anything about her."

"Go ahead."

"Well, that's just about all there is to it. I had this letter
delivered to me. It was addressed to Helen Framley, General
Delivery, Las Vegas. The post office just happened to catch it,
knew that I had an apartment here, and changed the address so that
it was delivered to this address."

There was a night light in a grocery store on a side street. It
gave sufficient illumination to see things—more or less clearly. I
stopped her in front of the window. "Let's see it."

"If Pug ever knew—"

"What business is it of his?"

"Really," she flared, "it isn't. I told him at the start it was
just a business partnership. He's insanely jealous. Of course, he
keeps wanting more—and then he hates the law. He says that it's
very evident there was some other Helen Framley in Las Vegas, just
passing through, and that I got a letter intended for her. I don't
know. I can't make it out, but Pug says I mustn't stick my neck
out."

"The letter."

"You promise you won't—"

"Hurry up," I said. "You haven't got all night. Neither have I.
Let's see it."

She opened her purse, took out an envelope, and handed it to me.

I put it in my pocket.

"No, no, you mustn't do that. I'll need the letter. Pug will ask me
about it as soon as I get back. He'll want to burn it."

"I'll have to go where I can read it and study it for a clue."

"Donald, you can't. You've got to just glance at it. I can tell you
what's in it. I— Oh, my God!"

I looked up, following the direction of her startled eyes. Pug was
standing on the corner of the main thoroughfare, looking up and
down the street.

She grabbed my arm. "Quick. Get back here—" Pug turned, looked down
the side street, saw us, took a dubious step forward as though
trying to see better, and then came rapidly toward us.

"What will we do?" she asked. "You run. I'll slick it out. Run fast
around the corner, and I'll delay things until— No, no, you can't.
Donald, he's dangerous. He's half. crazy. He—"

I held her arm as I walked toward him.

I couldn't see his face clearly. The hatbrim shaded the expression
in his eyes. The light on the side street was dim. A car swung
around the corner behind us. Its lights illuminated his face in a
harsh glare of white light. The features were hard with hatred.

Helen Framley saw that face and pulled back at my arm, twisting me
half around.

Pug didn't say anything. His eyes were on my face. He reached out
with his right hand, caught the girl by the collar of her jacket,
and sent her spinning across the sidewalk.

I swung for his jaw' I don't know whether it was the poor light or
whether he was too mad to see what 1 was doing, or sufficiently
disdainful not to care. He didn't try to block or dodge. My . blow
caught him on the chin. Unconsciously I'd remembered something of
what Louie had told me about throwing my body muscles into a blow.
I hit him so hard I thought my arm was broken.

It didn't even jar his head back on his neck. It was as though I'd
swung on the side of a concrete building. He said, "You
double-crossing, two-timing stool pigeon—" His fist crashed into my
jaw.

It was his left. It jarred me back on my heels. I knew his right
would be coming across. 1 tried to get out of the way and stumbled,
off balance, which threw my shoulder up. His right caught me on my
shoulder and sent me out across the sidewalk into the gutter.

The car swerved. Headlights blazed at us. I thought the machine was
going to run over me. I got up and Pug was coming toward me, not
hastily, just with a quiet deadliness of purpose.

The car was stopped now. I heard a door slam, steps behind me. A
voice said, "No, you don't,,"

Pug didn't pay any attention to the voice. His eyes were only on
me.

I thought I saw an opening and lashed out.

The big bulk of a body moved past me. I heard the thudding impact
of a fist against flesh, and then Pug and a big man were whirling
around in a tangled circle. The big - man's shoulder hit against me
and flung me off to one side. Before I could get back, Pug had
broken free. I saw his shoulders-weave, then the broad back and
huge shoulders of the big man interposed themselves between me and
Pug.

Something sounded like a fast ball thudding into a catcher's glove.
The big man came back hard, and took me down with him.

I heard people shouting. A woman screamed. There were steps—running
toward us.

Someone bent over us. I squirmed to get free. The automobile lights
showed Pug's face, still hard with cold hatred, bending over. He
jerked the inert body of the big man to one side as though it had
no weight. He leaned over me. His left hand grabbed my shirt and
necktie. He started to lift me.

Someone was back of him. I saw a club making a glittering half
circle, and heard the thud on the back of Pug's skull. The hand
that was holding my shirt loosened its grip. I fell back against
the bumper of the car.

By the time I straightened, there was a swirl of activity back in
the crowd. I heard the sound of grunting breaths, the sound of
another blow, then feet running, this time away from me.

The big man who had gone down and taken me with him struggled to
his knees. His right hand swung back to his hip. I saw blued steel
glittering in the light reflected by the automobile headlights. I
caught the man's profile as he raised the gun and turned his head.
It was Lieutenant Kleinsmidt.

A man pushed through the little crowd. "Everything all right,
Bill?" he asked.

Kleinsmidt said thickly, "Where is he?"

"He got away. I gave him a full swing with the club, but it didn't
stop him."

Kleinsmidt struggled to his feet.

I was tangled up with the bumper of the car. I had to get a hand on
it to pull myself up. Kleinsmidt grabbed me, spun me around, and
said, "Oh!"

I said, "I'm sorry, Lieutenant," and added with a flash of
inspiration, "I tried to hold him for you."

"You sure have guts," he told me, and rubbed his jaw. "What you
want him for, Bill?" the man with the club asked.

"Slot-machine racket," Kleinsmidt said, and then added as an
afterthought, "Resisting an officer."

"Well, we can get him."

Kleinsmidt said to me, "Know where he lives?"

I brushed dirt off my clothes. "No."

"Which way did he go?" Kleinsmidt asked.

Half a dozen people were eager to volunteer information. Kleinsmidt
looked back at the car for a moment as though hesitating, then
started out on foot, and took the other man with him. The little
crowd went streaming along behind to see the fun.

I limped away into the darkness. Seven o'clock, and Bertha would be
waiting.



