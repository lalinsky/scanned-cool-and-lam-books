## Chapter Eight

CHIEF LASTER GLARED AT ME across his desk and said, "Sit down."

I pulled up a chair and sat down. Kleinsmidt settled himself over
on the far side of the room, and crossed his legs.

Daylight was just breaking outside the building. The streamers of
eastern clouds were a vivid crimson-orange, giving a reddish tinge
to the landscape and even causing a slight russet coloring on the
chief's face. There was just enough light outside to make electric
lights seem sickly and pale, but not quite enough to dispense with
artificial illumination.

Laster said, "Your name is Donald Lam, and you claim to be a
private detective."

"That's right."

"Working for the B. Cool Detective Agency."

"Yes."

"Now, you hit town yesterday afternoon on a plane, didn't you?"

"Yes."

"Right away, you started stirring up a lot of trouble.'"No.”

He raised his eyebrows. "No?" he asked sarcastically. "No. A lot of
trouble started stirring me up." He stared at me to see if I was
cracking wise.

"Well, you involved Lieutenant Kleinsmidt in a fight, had an
argument with the attendant in charge of the slot machines at the
Cactus Patch, then had a street fight with a man by the name of
Beegan, didn't you?"

I said, "The attendant over at the Cactus Patch took a swing at me.
He called for the police. Lieutenant Kleinsmidt answered. As far as
the other is concerned, a man made an unprovoked assault upon both
Kleinsmidt and me. Kleinsmidt really got going and this man beat
it—fast."

I glanced at the Lieutenant out of the corner of my eye. He was
grinning. He liked that version of the fight.

Laster tried another approach. "You called on Helen Framley
yesterday, didn't you?"

"Yes."

"How did you get her address?"

"A client of the agency gave it to me."

He started to say something, ,changed his mind, consulted some
notes on his desk, looked up suddenly, and said, "Harry Beegan was
her boy friend, wasn't he?"

"I wouldn't know."

"He acted like it?"

"I'm afraid I'm not qualified to judge."

"You were on that train for Los Angeles that leaves here at
nine-twenty?"

"That's right."

"You got aboard by the skin of your eyeteeth, did you not?"

"I did not."

"What time did you get aboard?"

"As soon as the train pulled in."

"You mean you were waiting at the station and got aboard the train
just as soon as it stopped?"

"That's what I said."

"Now, Lam, think that over carefully, because your answer may make
quite a difference."

"To whom?" I asked:

"To you, among other people."

"I fail-to see any reason for thinking over carefully what time I
took a train."

"You're going to stick to that story?"

"That's right."

"You didn't catch the train just before it was pulling out?"

"No."

"You didn't board it after the train had been standing in the depot
for some time?"

"No."

"You got aboard just as soon as the train came to a stop?"

"OW I waited for the other passengers to get off. That took a
minute or two."

"But you were standing there by the train, waiting for the other
passengers to get off—"

"That's right. What's all this leading up to?"

"I want to find out a little more about that train first. You were
in the depot at nine-o-five?"

"I was at the depot by nine o'clock." •

"Where in the depot?"

"I was standing out where it was cool."

"Oh," he said as though he'd trapped me into some damaging
admission, "you weren't in the depot?"

"Did I ever say that I was?"

He frowned. "You were waiting outside?"

"That's right."

"How long before the train came?"

"I don't know. Five minutes, perhaps ten."

"See anyone you knew out there?"

"No."

The chief looked over at Kleinsmidt. "Bring the Clutmers' in,
Bill."

Kleinsmidt went out through a door that opened on a .corridor. I
said to the chief, "Now that I've answered your questions, perhaps
you'll tell me what this is all about."

A moment later, the do& opened, and the woman who had been in the
apartment adjoining Helen Framley'swalked into the room; a step
behind her came her husband. ,They looked as though it had been a
hard night. Their eyes were red-rimmed. The muscles on their faces
sagged.

The chief said, "You know Mr. and Mrs. Clutmer?"

"I've seen them."

"When did you see them last?"

"Yesterday."

"What time yesterday?"

"I don't remember."

"Did you see them after eight-thirty last night?"

"No."

The chief said to Clutmer, "This man claims to have been hanging
around the depot waiting for the nine-o-five train to come in. How
about it?"

It was Mrs. Clutmer who answered the question. "That's absolutely
impossible. I told you he couldn't have been there. The only way he
could have caught that train was by the skin of his eyeteeth,
because we didn't leave the platform until just as the train was
ready to pull out."

"You're sure he couldn't have been there?"

"Absolutely positive. We had been talking about him, and I'd have
noticed him if he'd been there."

"What time did you get to the depot?"

"Five or ten minutes to nine I think it was. We had to wait about
ten minutes for the train to come in, and it was on time."

Laster looked at me. 'There you are.”

I said, "Mind if I smoke?"

He frowned. Kleinsmidt smiled.

Laster said to Mrs. Clutmer, "This man says he was on the outside
of the depot standing out where it was cool, waiting for the train
to come in. Now where were you?"

"We were right inside the depot for a while, and then we went out
and stood on the platform outside. But we watched the people get
off the train, and we watched the people who got on. Not that I'm
nosey at all, but I just like to know what's going on. I just use
my powers of observation, that's all."

Laster turned to me.

"Well?" he asked.

I lit a match, held it to the end of my cigarette, and took a deep
drag.

Mrs. Clutmer started volunteering information. "Helen Framley is
pretty strong for this young man, if you, ask me. I know for a fact
that she and that boy friend of hers had a quarrel over this man
last night."

"How do you know it was over him?" Laster asked.

"You could hear what they said in my apartment just as plain as
day. They were talking very, very loud. Their voices were
raised—almost shouting at each other. He accused her of falling for
him, and she said that if she wanted to -she would, that Beegan
didn't have any mortgage on her. Then Beegan said he'd show her
whether he had a mortgage on her or not, and said she'd spilled a
lot of information she had no business giving. Then he used some
kind of a funny expression—that is, he called her something."

It was Clutmer who furnished the gap in the information. "Called
her a stool pigeon," he said dryly, and then lapsed into silence.

"You hear that, Lam?" the chief asked.

"I hear it."

"All right, now what have you to say?"

"Nothing."

"You're not going to deny it?"

"Deny what?"

"That they fought over you."

"I wouldn't know."

"And you still claim you were down at the depot?"

"I've told you where I was."

"But these people say you couldn't have possibly gotaboard that
train immediately after it pulled into the station."

"I heard them."

"Well, what about it?"

"They're entitled to their opinion—that's all it is. I was-on the
train."

Mrs. Clutmer said, "I'm absolutely positive!"

Kleinsmidt said, "Just a minute, Mrs. Clutmer. You went down there
to meet some people who were coming through, didn't you?"

"Yes."

"Friends from the East, I believe?"

"Yes."

"You were looking forward to seeing them?"

"Naturally. What do you suppose we went down there for?"

"And you were excited?"

"I don't think so."

"You knew what time the train got in?"

"Yes."

"What time did you leave your apartment?"

"About twenty Minutes to nine."

"And walked to the depot?"

"Yes."

"That puts you there fifteen minutes before train time?"

"That's right. That's what I'm telling you. We were there. If
anyone had been there, we'd have seen them."

"Why did you go to the depot so early?"

"Well, we wanted to be certain that we met the train."

"You knew it would be there for fifteen minutes? You were pretty
excited over seeing these old friends of yours?"

"Well, we were looking forward to it."

"And as soon as the train pulled in, you started looking for
them?"

"Well, we looked over the people all right."

"Where were your friends?"

"Standing right in the vestibule."

"And you had quite a little talk-fest there on the station
platform?"

"We visited and chatted."

"Your friends couldn't stop over?"

"No. They were going to Los Angeles on business. They were with
some other people."

"And you visited until the conductor called out 'All aboard'?"

"Yes."

"Then they got back aboard the train?"

"Yes."

"Now, did you wait for the train to pull out, or did you leave
then?"

"We left then, but the train pulled out right afterwards' We heard
it pulling out just as we walked through the depot. And I may say
this. We waited until after the porter had closed the vestibule."

"On the car in which your friends were riding?"

"Well—yes."

Kleinsmidt looked at the chief, didn't say anything.

The chief frowned at me, then looked at Mrs. Clutmer his eyes
shifted past her to her husband. "What's your name—your first
name?"

"Robert."

"You were with your wife?"

"Yes."

"Do you agree with everything she says?"

"Well—well-1444—in a way, yes."

"Where don't you agree with her?"

"Oh, I agree with her all right."

"Do you think there's any possibility this young man ould have been
at the depot and you not seen him?"

"Well, now there's just a chance—just a bare chance."

"Would it," I asked, "be too much to ask what this is all about?"

Mrs. Clutmer said, "Why, don't you know? They—"

"That'll do, Mrs. Clutmer. I'll handle this," the thief
interrupted.

She glared at him and said, "Well, you don't need to snap a
person's head off. I was only going to tell him—"

"I'll tell him." -

"Well, he can read it in the papers. I guess there's no great
secret about it. I—"

The thief made a motion to Kleinsmidt. He pushed his big frame up
out of the chair, said to the Clutmers, "All right, folks, that's
all."

"Let 'em go home," the thief said.

"You can go home now," Kleinsmidt told them.

"Well, I should say it was about time! The idea of getting a body
up at midnight and keeping her--"

"Get 'em outside," the chief roared.

Kleinsmidt pushed them through the door and pulled the door shut
behind them.

The chief turned to me. "Doesn't look too good for you, Lam."

"Apparently, someone was killed. Who was it?" Lieutenant Kleinsmidt
opened the door, entered the room, and pushed the door shut.

Chief Laster stared down at some notes in a leather-backed book
which was open on his desk. Then he took a pen from his pocket and
scribbled a few more notes.

He looked up, screwed the cap back on the pen, put it in his
pocket, and said, "Harry Beegan was shot and killed last night
sometime between quarter to nine and nine-twenty-five."

"Too bad."

They both looked at me. I didn't say anything more and didn't give
them any facial expression to read.

"The girl he was living with seems to have skipped out," Chief
Laster went on.

"Was he living with her?"

"Well, he was there a lot."

"There's quite a difference," I said.

"A very few minutes before he was killed—sometime within two hours
let us say of the time of his death—you called on this girl. Beegan
entered the picture. You had an argument. You left. Beegan accused
the girl of having fallen for you. He was jealous. He accused her
of going out to meet you. She swore she wasn't going to do anything
of the sort. She went out. She met you. Beegan followed. You had a
fight over the girl. I think it's fair to surmise that you arranged
with her to run away from Beegan and meet you in Los Angeles. She
left to keep that rendezvous."

"I don't follow your reasoning."

"You were working on a case. Your employer was here. You had
planned to stay here for two or three days."

"Who says so?"

"It's a fair inference. Mrs. Cool is still here."

"The job I'm working on is finding someone who disappeared from Los
Angeles. That's where the trail starts, and that's a mighty good
place to pick it up."

He ignored me. "Suddenly last night out of a clear sky, you
announced you were going to Los Angeles on the first available
train. You left the Sal Sagev Hotel, which is right at the depot,
with lots of time to spare. You had every motive, every incentive,
and every opportunity to shoot Harry Beegan, and you know that as
well as I do."

"He was shot in the girl's apartment?" I asked. "Yes."

"How do you fix the time so accurately and yet still have an
indefinite interim period?"

—The Clutmers were in their apartment until they went down to the
station to meet some friends who were coming through. They left the
train and walked directly back to their apartment. They hadn't
heard anything at all—no sounds coming from the next apartment.
They'd have heard voices raised in an argument.
Unquestionably,they'd have heard a shot. That fixes the time of the
murder absolutely within those limits."

"Unless the Clutmers are lying."

"Why should they lie?"

Suppose they didn't like this man, Beegan, and had been waiting for
an opportunity to do something about it? When was the body
discovered?"

"Shortly before midnight."

"All right, suppose they came home, found Beegan either standing in
the door of the girl's apartment or in the hallway or on the
stairs? They had an argument. Or else they just walked into the
apartment behind him and took a pot shot at him. If you list them
as suspects, the killing could have been any time before the body
was discovered."

"It sounds silly."

"Perhaps it does to you. It sounds silly to me to think that I'd
have shot him."

"You wanted his girl."

"No more than I want a couple of hundred other attractive women."

"Enough to run risks of taking a beating."

"I was working."

"I know," he said, and ran the tips of his fingers along the angle
of his jaw, "you have a great devotion to duty."

"When I work on a case, I want to crack it--the same as you do."

"Well, as far as that is concerned, the Clutmers are out of it.
That means that the time of the murder stands. Now come on, Lam,
let's be fair about this. If you were going to have a meeting with
the girl, we'll find it out. If that's all there was to it, we'll
forget it. But you know good and well that's why you wanted to go
to Los Angeles. Now, isn't it?"

"I don't get you." •

"You fixed it up with the girl to meet you in Los Angeles."

"No."

He said, "That denial just doesn't register with me."

"That's okay. Too bad you dragged me off the train."

"What do you mean?"

"I'm only a private detective," I said. "I don't want to tell you
how to run your business, but if you had let me go on to Los
Angeles and put a shadow on me and if I'd met the girl there, then
you'd really have had something. As it is now, you can't prove I
was going to meet the girl."

"It's a fair inference."

"Nuts!"

Laster said, "There's one other highly suspicious circumstance.
When Kleinsmidt asked you if you knew where Beegan lived, you told
him you didn't."

"That's right. I didn't."

"But you'd already been up to the apartment.'"He didn't live
there."

"His girl friend did."

"That wasn't what Lieutenant Kleinsmidt asked me."

"Aren't you being rather technical?"

"He asked me if I knew where Beegan lived."

"Well, you knew what he meant."

"And because I knew where his girl friend lived and didn't tell
Kleinsmidt, you think I was holding out?"

"Yes."

"I saw no reason to drag the girl into it."

Laster said, "That's all for the present."

"I can go now?"

"Yes."

"I want to go to the Sal Sagev Hotel."

'Well, you can.”

1 see no reason why I should walk. Remember, I was taken off a
train to Los Angeles. I had my transportation and berth all paid
for. What are you going to do about that?”

'Laster thought for a minute, then said, "Nothing."

"1 want to get back to Los Angeles."

"Well, you can't leave until we've finished our investiga tions."

"When's that going to be?"

"I don't know."

I said, "I'll report to Bertha Cool. if she tells me to go to Los
Angeles, I'm going."

"I'm not going to permit it."

I said, "If I'm locked up, I won't go. If I'm not locked up, I'm
going. How about having the Lieutenant take me to the Sal Sagev
Hotel?"

Laster said, "Don't be silly. It's only a couple of blocks. You're
a cool customer, Lam. Kleinsmidt told me as much, but—"

"Nuts. I'm giving you all the breaks. I could make you send me back
to Los Angeles. I may yet, after I've talked with Bertha Cool.
Right now, all I'm saying is that I want to go to the Sal Sagev
Hotel."

Kleinsmidt got up from his chair. "Come on, Lam," he said.

There was a police car outside. Kleinsmidt was grinning as I got
in.

"Well?" I asked.

"I told him to let you go through to LOS Angeles, have the Los
Angeles police pick up your trail, see if you met the girl, and if
you did to pinch you both; otherwise, to leave you alone. He
wouldn't listen to me. He said it was a cinch you were the one who
shot him, that from all reports, you were a pencil-necked little
chap who would spill every thing you knew if we jerked you off the
train, rushed you back here, and didn't do any talking on the way."
I yawned.

Kleinsmidt's car slid smoothly through the streets, deposited me at
the Sal Sagev Hotel.

"How about you, Lieutenant?" I asked.

"What do you mean?"

"What were you doing last night between eight-fortyfive and
nine-twenty-five?"

"I was -hunting for Beegan."

"Didn't find him, did you?"

"Go to hell," Kleinsmidt said, and grinned.



