## Chapter Five

WE rode back to the office in silence. I put the car in the parking
lot and we rode up in the elevator, went into the office, and sat
down.

Bertha Cool looked across at me and said, "How did you find out
she'd been murdered, lover?"

I said, "What the devil are you talking about?"

Bertha Cool scraped a match on the underside of the desk, lit her
cigarette, looked at me, and said, "Nuts."

She smoked for a while in silence, then she said thoughtfully, "Cop
cars were scattered all around the joint. You pretended not to see
them. You didn't want to ring her apartment. You wanted to ring the
manager. You went on up, asked a couple of questions, turned
around, and went back down. You knew something had happened. What
you wanted to find out was whether the police were there. Going to
tell me about it?"

"There's nothing to tell."

Bertha Cool opened a drawer, took out a card, looked at the number
on the card, picked up the telephone, and dialled a number. When
the party at the other end of the line answered, she said, in that
cooing voice of hers, "Mr. Donald Lam has a room at your place I
believe, Mrs. Eldridge. This is Mrs. Cool, head of the Cool
Detective Agency. Donald works for me, you know. I'm very anxious
to find him. Do you know if he's in his room?"

Bertha Cool listened while the receiver made noises, then she said,
"I see. About an hour ago, eh? Well, can you tell me if someone
called on him shortly before he went out?" Again she listened, and
said, "Oh, yes, I see. Can you describe her, please?"

Again Bertha Cool listened, her lids half closed. Beneath them her
cold, grey eyes shifted to glance at me, then she said, "Thank you
very much, Mrs. Eldridge. If he comes in, tell him I was trying to
reach him, will you?"

She hung up, pushed the telephone back across the desk, turned to
me, and said, "All right, Donald. Who was she?"

"Who?"

"The girl who came to see you."

"Oh," I said. "That was a girl who went to law school with me. I
hadn't seen her for a long time. She heard I was working for you
and rang up this afternoon to get the address. Elsie gave it to
her.' Bertha Cool smoked for a while, then she dialled another
number and when she got an answer said, “Elsie, this is Bertha. Did
someone ring up and ask for Donald's address this afternoon? … Who
was she? Did she leave her name? … Oh, he did, eh? All right,
Elsie. Thanks. That's all."

Bertha hung up the telephone and said, “You told Elsie you hadn't
seen this girl.

I said, "All right. Have it that way if you want. I don't believe
in letting Elsie Brand in on my love life. This girl was a pal of
mine. She ran up and chatted with me for half an hour or so. It was
purely social."

"Purely social, eh?" Bertha Cool asked.

I didn't say anything.

Bertha Cool smoked some more, and said, "All right, lover. We'll go
get some supper. This isn't agency business. It's Dutch treat."

"I'm not hungry," I said.

She smiled. "Oh, well, I'll be generous, Donald. We'll put it on
the expense account."

I shook my head. "I don't want anything."

"Well, you can come along and keep me company."

"No, thanks. I want to think."

"Think while you're with me, lover."

"No. I can stay here and figure things out a lot better."

Bertha Cool said, "I see." She pulled the telephone over towards
her, dialled a number, and said, "This is B. Cool. Send me up a
double clubhouse sandwich and a quart bottle of beer." She hung up
the telephone, said, I'm sorry you're not hungry, Donald. Bertha
will sit right here and wait with you.

I didn't say anything.

We sat there in silence, Bertha Cool studying me with half-closed
eyes, smoking thoughtfully. After a while there was a knock on the
door, and Bertha Cool said, "Open it and let the waiter in."

The waiter from the restaurant downstairs brought in a tray with a
double clubhouse sandwich and a quart of beer. Bertha Cool told him
to put it on the desk, paid him, gave him a tie, and said, ” You
can get the dishes in the morning. Were going to be busy tonight.”

The waiter thanked her and left. Bertha munched on the sandwich,
washed it down with big gulps of beer, and said, "It's a hell of a
way to make a dinner, but it will stay my appetite. Too bad you
weren't hungry."

After she'd finished and had another cigarette, I looked at my
watch carelessly and said, "Well, I guess there's no use waiting
any longer."

Bertha Cool beamed at me. "I guess you're right. Who was she? Why
did she stand you up on the call?"

"A swell jane," I said. "She was going to ring me up for a dinner
date. Can't a man step out with a girl friend without having the
whole damned office force trying to chisel in on his love life?"

"Apparently not," Bertha Cool said placidly. "All right, if you
want to go, we'll go."

We went down and climbed in the agency car again. I said, "Well, I
might as well go to a picture show and kill time that way. Do you
want to go?"

"Hell, lover. Bertha's tired. She'll just go to her apartment and
get her clothes off and read a book."

I drove her to her apartment. She got out and put a jewelled hand
on my left arm. "I'm sorry," she said.

"It's all right," I told her. "The jane didn't call up. I guess she
must have called while we were out, and probably some other guy was
waiting to begin where I left off."

"Oh, well, Donald, there are lots of women. A young, good-looking
boy like you won't have any trouble on that score. Good night."

"Good night," I said.

I turned the agency car and made time back to the office. I looked
at my wrist watch. I'd only been gone twenty-five minutes in all. I
hoped Marian hadn't called in during that time.

I sprawled out in a chair and was just lighting a cigarette when I
heard the sound of a key in the lock of the door. I thought it was
the janitor and called out, "We're busy. Let it go until tomorrow,
will, you, please?"

The latch clicked back and Bertha Cool, calmly placid, came walking
into the office. She smiled at me and said, "Thought so," then
sailed on through the entrance office to seat herself in the big
swivel chair behind her desk. She said, "You and I could get along
a hell of a lot better, Donald, if we didn't try to slip things
over on each other."

I was just starting to answer that when the telephone on Bertha
Cool's desk started to ring. Bertha, with a scooping motion of her
thick right arm, pulled the tele p hone towards her, picked up the
receiver, and said, Hello. ” Her eyes were on me, half-closed eyes
that glittered like diamonds. Her left arm was out across the desk
ready to stiff-arm me back in case I made a lunge for the
telephone.

I sat still and smoked.

Bertha Cool said, "Yes, this is Bertha Cool's agency… . No, dear,
he isn't here right now, but he told me you were going to telephone
and said I'd take the message … Oh, yes, dearie. Well, he expects
to be here in just a few minutes. He said for you to come right up
.. . Yes, that's right. That's the address. Come right up, dearie.
Don't waste any time. Get a taxicab. He wants to see you."

She dropped the receiver back into place and turned to face me.
"Now then, Donald," she said, "let this be a lesson to you. The
next time you try to cut yourself a piece of cake, cut Bertha in on
it, otherwise there's going to be trouble."

"You want in on it, do you?" I asked.

"I'm in," she said.

I said, "You are, for a fact."

She said, "You came to work for me, lover, a little runt that
didn't know anything about the detective business. I picked you up
when you were down to your last cent. You hadn't eaten for two days
when you came to the office. I gave you a job. You're learning the
business. You have brains. The trouble is, you don't keep in mind
that I'm the boss. You get to thinking you're running the business.
It's a case of the tail wagging the dog."

"Anything else?" I asked.

"Isn t that enough?" Bertha asked.

"It's plenty," I said. "Now would you like to know what you've cut
yourself in on?"

She smiled and said, "Well, that might help some. No hard feelings,
eh, Donald?"

"No hard feelings," I said.

Bertha said, "I stick up for my rights. When I have to fight, I
fight to win. I don't hold any grudges. I fight because I want to
accomplish something. When I've accomplished it, that's all I
ask."

"She's coming up here?" I asked.

“Right away. She said she had to see you right away.

It didn't sound like a date to me, lover. It sounded like
business.”

"It is business."

"All right, Donald. Suppose you tell Bertha what it is. I've
declared myself in on the deal, so I'd like to see what cards are
in my hand — and what the stakes are. But don't forget that I hold
the trumps."

"All right," I said. "You're in on a murder."

"I knew that already."

I said, "The girl you were talking with was Marian Dunton. She was
stranded in a hick town up in the foothills. She wanted to get out.
She played a hunch that this Lintig case was something bigger than
appeared on the surface. She followed my back trail and got a lead
by which she figured she could dig up some information."

"You mean with this Evaline girl?"

"Yes."

Bertha said, "Never mind the history. I figured that all out
myself. Tell me something I don't know."

I said, "I don't know just what time the post-mortem will show
Evaline Harris was murdered, probably about the time Marian Dunton
went to her apartment for the first time."

"For the first time?" Bertha asked.

"Yes. She opened the door of the apartment and saw Evaline lying on
the bed. She thought she was asleep. A man had just left the
apartment. Marian thought it wasn't exactly a propitious moment for
getting information, so she quietly closed, the door and went back
to sit in the car where she could watch the door of the apartment
house. After half an hour or so, she tried it again. She was bolder
that time and more curious. She found Evaline Harris had a cord
knotted around her neck and was quite dead. Marian lost her head,
could only think of me, and came rushing up to my room to tell me
about it. I sent her to the police, told her to say nothing about
having been to me, nothing about the agency, nothing about Mrs.
Lintig, simply that she was approaching Evaline to see about
getting a job in the city, that she thought Evaline was asleep the
first time, and had gone out to wait in the car."

"I doubt if she gets away with that," Bertha Cool said. "I think
she will."

"Why?"

"She's from the country. She's a simple, unsophisticated, darn nice
girl. It sticks out all over her. She's fresh and unspoiled. She
hasn't learned the chiselling tactics of the city. She's just a
square-shooting good kid."

Bertha Cool sighed and said, "That's one of your greatest
weaknesses as a detective, lover. The women all knock you for a
loop. You fall for them head over heels. The fact that you can't
get anywhere in a fight is bad enough, but this business of falling
for women is twice as bad. You've got to learn to quit it. If you
could only do that, your brains would get you places."

“Anything else?' I asked.

Bertha Cool smiled and said, "Now, don't be like that, Donald. This
is business, you know."

"All right," I said. "Now I'll tell you the rest of it. Marian got
a pretty good look at the man who was coming out of the apartment.
Her description won't mean anything to the police – at least, I
hope it won't – but it meant something to me."

"What do you mean?"

"The man who left the apartment," I said, "was Dr. Charles Loring
Alftmont, otherwise known as Dr. James C. Lintig. He prefers to
have us refer to him as Mr. Smith."

Bertha Cool stared at me. Her lids slowly raised until her eyes
were round and startled. She said, so softly that it was almost
under her breath, "I'll be stewed for an oyster."

"Now then," I said, "the police don't know anything about the
Lintig angle. They don't know anything about the Alftmont angle.
There's no particular reason why they should suspect the man whom
we will refer to as Smith from now on. But if Marian Dunton should
see him or should see his photograph, she'll identify him in a
minute."

Bertha Cool gave a low whistle.

"Therefore," I said, "you can play things in either one of two
ways. You can turn her loose on her own, in which event the police
will sooner or later get a lead to Smith, put him in a line-up, and
ask Marian Dunton to identify him, in which event the fat will be
in the fire and you won't have any client; or you can keep Marian
out of circulation as much as possible, tell Smith what we know,
make him give us his side of the story, tell him we're standing
between him and a murder rap, get unlimited funds with which to
work, and try to clean the thing up."

"Won't that be suppressing evidence, lover?"

"Yes."

"That's serious, you know, for a private detective agency. They'd
hook me for my licence on that."

“If you hadn't known anything at all about it, they couldn t have
held you responsible.

"Well," she said, "I know about it now."

"Yes," I said. “You've cut yourself in on the deal. Marian ‘s on
her way up here. It's your play. You know what the cards are now.

Bertha Cool pushed back her chair. "Forgive me, Donald," she said.
"I'm going to get the hell out of here."

"No, you aren't," I said. "You answered the phone, and told her to
come. I wouldn't have done that. I'd have told her to go to the
Union Depot or some place like that, and I'd have met her there.
She's probably under surveillance."

Bertha Cool started to drum with her thick, jewelled fingers on the
top of the desk. "What a mess," she said. You cooked it,' I told
her.

"I'm sorry, Donald."

"I thought you would be."

“Listen, couldn't you take over and – “

"Nothing doing,' I said. “If you hadn't known anything about it, I
could have gone ahead and done what I thought was necessary. I
could have acted dumb and if anyone had - questioned me, they could
never have proved anything except that I was dumb. Now, it's
different. You know. What you know might get found out."

"You could trust me, lover," she said.

"I could, but I don't."

"You don't?"

"No."

Her eyes hardened and I said, "No more than you trusted me a few
minutes ago."

There was a timid knock at the outer door. Bertha Cool called,
"Come in."

Nothing happened. I got up and crossed through the reception-room
to open the door. Marian Dunton stood on the threshold.

"Come in, Marian," I said. "I want you to meet. the boss, Mrs.
Cool, this is Miss Dunton."

Bertha Cool beamed at her. "How are you?" she said. "Donald has
told me the nicest things about you. Do come in and sit down."

Marian smiled at her, said, "Thank you very much, Mrs. Cool. I'm
very glad to meet you," and then came to stand close by me. She
gave my arm a quick, surreptitious squeeze. Her fingers were
trembling.

“Sit down, Marian, I said..

She dropped into a chair.

"Want a drink?"

She laughed and said, "I had one."

“When? ‘

"After they got done with me."

"Was it bad?"

"Not particularly." She glanced significantly at Bertha Cool.

I said, "Mrs. Cool knows it all. Go ahead and tell us."

“Does she know about — about — “

"You mean about your coming to my place?"

"Yes."

"She knows it all. Go ahead, Marian. What happened?"

She said, "I got away with it nicely. I went to the police station
and told them I wanted to report a body, and they sent me into the
traffic department. Evidently they thought it was an automobile
accident. I had to do quite a bit of explaining to two or three
different people. They sent a radio car to investigate, and the
radio officers called in for Homicide. After that there was a lot
of activity, and a nice young district attorney took a statement
from me."

"Did you sign it?" I asked.

"No. It was taken down by a stenographer, but they didn't type it
out. They didn't ask me to sign it." I said, "That's a break."

"Why? I couldn't go back on anything I'd said."

"No. But the fact that they didn't tie you up with a signature
shows they are taking your story at its face value."

She said, "Mostly they were interested in this man who was leaving
the apartment."

"They would be," I said.

"They tried to convince me that I'd really seen him coming out of
the door of 309, and tat I mustn't say anything to anyone about
thinking he might have been coming from another apartment."

"I see."

She went on : "The young deputy district attorney was very nice. He
explained that in order to convict a man of murder it was necessary
for the evidence to show his guilt beyond all reasonable doubt.
Well, you know how it is, Donald. There's a lot of question as to
when a doubt is reasonable. Of course, the man might have been
coming from another apartment, but it didn't look like it, and, the
more I think of it, the more I'm certain he came from apartment
309. Now then, if I should just make some slip which would indicate
I wasn't dead sure of what I'd seen, a shyster attorney,
representing the murderer, would use it to cheat justice. After
all, Donald, a citizen has quite a responsibility, and a witness
must be willing to take the responsibility of telling things the
way he saw them."

I smiled and said, "I see it was a very nice deputy district
attorney."

"Donald, don't be like that. After all, really those are the
facts."

I nodded.

"The police are going to find out all about Evaline Harris. They're
going to find out who her men friends were, and after they learn
about them, I'll be called on to make some identifications,
probably first from photographs."

"They think it was a boy friend?" I asked with a significant glance
at Bertha Cool.

"Yes. They think it was a crime of jealousy. They think that the
man who did it had been — well, you know, a lover. You see, the
body was lying nude on the bed, and there was no evidence of a
struggle. The man must have slipped the cord around her neck and
drawn it tight before she knew anything was happening."

"What are you supposed to do?" I asked. "Stick around here or go
back to Oakview?"

"I'm supposed to be available," she said. "They investigated me.
They telephoned the sheriff at Oakview, and the sheriff is an old
friend of mine. He said they could trust me anywhere any time."

"Did they," I asked, "act as though they thought you might have
been the one who did it?"

No. Coming to the police station and all that was in my favour, and
I acted just the way you told me to — you know, hicky and
countrified.”

"That's swell," I said. "How about dinner, Marian? Have you
eaten?"

"No, and I'm so hungry I could eat a horse."

I grinned at Bertha Cool and said, "Too bad you've eaten, Mrs.
Cool. I'll take Marian out to dinner. I'll want some expense
money."

Bertha Cool positively beamed. "Yes indeed, Donald," she said. "Go
right ahead and take her out. There'll be nothing for you to do
this evening."

“I want some expense money.'

“Just be sure to be on deck at nine o'clock in the morning, Donald,
and if anything turns up tonight, I'll call you.

"That's fine. And the expense money?"

Bertha Cool opened the drawer in the desk. She opened her purse,
took out a key to the cash drawer, counted out a hundred dollars in
bills, and handed them to me. I kept my hand extended and said,
"Keep coming. I'll tell you when to stop."

She started to say something, then handed me another fifty. "That,"
she said, "is all that's in the drawer. I don't keep any more cash
than that in the office." She slammed the lid of the cash box shut,
locked it, and closed the drawer.

I said, "Come on, Marian."

Bertha Cool positively beamed at us. "You two go ahead," she said,
"and enjoy yourselves. I've had dinner. It's been a hard day, and
all I want right now is to get home where I can get into some
lounging pyjamas and relax. I guess I'm getting old. A hard day
uses me up and leaves me limp as a dishrag."

"Nonsense," Marian said. "You're a young woman."

"I have to carry all this fat around with me," Bertha explained.

"It isn't fat. It looks like muscle," Marian insisted. "You're
big-boned, big-framed, that's all."

"Thank you, my child."

I took Marian's hand and said, \`Let's go, Marian.”

Bertha Cool locked the desk, dropped the key in her purse, got to
her feet, and said, ‘Don't bother about taking me home, Donald.
I'll go in a cab.”

She walked across the office with us with that peculiar, effortless
walk of hers which seemed as smooth as the progress of a yacht on a
calm sea. Bertha never waddled. She didn't make a hard job of
walking. She moved with short steps, never hurrying herself, but
keeping up a steady pace regardless of whether it was hot or cold,
up' hill or down.

When we were in a restaurant, Marian said, "I think she's
wonderful, Donald. She seems so competent, so self-reliant."

"She is," I said.

"She looks hard, though."

"You don't know how hard," I said. "And now, let's talk about
you."

"What about me?"

"Why did you leave Oakview?"

"To see Evaline Harris, of course."

"Did you tell your uncle that?"

"No. I told him I wanted to take a part of my vacation."

"I thought he'd gone fishing."

"He came back."

"When did he come back?"

She puckered her forehead and said, "Let me see. It was — it was
right after you left."

"How long afterwards?"

"Just a couple of hours."

“Y

"And you started for the city as soon as he got back?" es.” I said,
"All right. Now, where do you fit into the pic ture?"

"What do you mean?"

"You know what I mean. You said that if I wanted to pool
information with you, you'd put in with me, that if I didn't you
were going on your own."

She said, "You know all about that."

"All about what?"

“About how I felt. I want to get out of that newspaper. I want to
get out of Oakview. I knew that you were a detective —

"How did you know?"

"I'm not blind," she said. “You had to be a detective. You were
working for someone. You were trying to get information, and you
weren't simply trying to look up a bad credit or collect a bill —
not after twenty-one years.'

"All right, go ahead."

“Well, I knew you were a detective, and I knew Mrs.

Linig was mixed up in something big. There's been too much interest
in her, and I figured you got that black eye because of trying to
find out about her. So I figured if she was that important, it
would be a good chance for me, being on the ground, to get in on
the ground floor, use my acquaintanceship in Oakview to find out
what it was everyone was after, and find out who you were working
for, go to your boss with the information, and see if I couldn't
get a job.”

"What kind of a job?" I asked curiously.

"Being a detective. They have woman detectives, don't they?"

I said, "You were going to ask Bertha Cool to give you a job as a
detective?"

"Yes. Of course I didn't know anything about Bertha Cool at the
time. I didn't know who your boss was. I thought probably it was
one of the big agencies or something like that."

"What do you know about being a detective?"

“I've had to do reporting up there in Oakview, and even if it is a
little paper in a one-horse country town, you have to have a nose
for news to get by. I'm ambitious and – well, they can't rule you
off the track for trying.

I said, \`Forget it. Go back to Oakview and marry Charlie. By the
way, how is Charlie?”

"All right," she said, avoiding my eyes.

"What did he think of you leaving Oakview and coming to the city to
get a job being a detective?"

"He didn't know anything about it."

I kept watching her, and she, feeling my eyes on hers, kept looking
at the tablecloth. I said, I hope you're telling me the truth.”

She raised her eyes then in a quick flash, and said, "Oh, but I
am." Then she lowered her eyes again.

A waiter took our orders, and brought food. Marian didn't say
anything until after she'd finished her soup, then she pushed the
plate away, and said, "Donald, do you suppose she'd give me a
job?"

"Who?"

"Why, Mrs. Cool, of course."

"She has a secretary," I said.

"I mean as. a detective."

"Don't be silly, Marian. You couldn't be a detective."

"Why not?"

"You don't know enough about the world. You have ideals. You — its
silly to even think of it. Bertha Cool takes all sorts of cases,
divorce cases particularly."

"I know the facts of life," Marian Dunton said indignantly.

I said, "No, you don't. You just think you do. What's more, you'd
feel like a heel. You'd have shadowing jobs. You'd be snooping
around, peeking through keyholes, digging down into the muddy dregs
of life – things you shouldn't know anything about."

"You talk like a poet, Donald," she said, and tilted her head
slightly on one side as she looked at me. "There's something poetic
about you, too," she went on. "You have that sensitive mouth, big;
dark eyes."

I said, "Oh, nuts."

The waiter brought our salads.

I kept looking at her, and she kept avoiding my eyes. I waited for
her to talk, and she didn't feel like talking. After a while she
looked up at me and said, "Donald, do you know that man who was
coming from Evaline Harris's apartment?"

Her eyes held mine then, steady and searching.

I said, "That's what comes of being coached by the police."

"What do you mean?"

"When you told me about it the first time, you didn't say that he
was coming from that apartment. You said he was coming down the
corridor."

"Well, he came out of an apartment."

"But you didn't know that it was Evaline Harris's apartment."

"It must have been."

"Do you think so?"

“Yes.

"Do you know that it was her apartment?"

"Well – well, not exactly, but it must have been, Donald."

I said, "Tomorrow, after things have quieted down, we'll go up to
that apartment house. You come out of the elevator. I'll stand in
the doorway of apartment 309 and step out into the corridor just as
you leave the elevator. Then we'll try it with the other two
doorways."

She squinted her eyes thoughtfully and said, "Yes, it might work.
Perhaps Mr. Ellis would like to have me do that for him."

"Who's Ellis?"

85

"Larchmont Ellis, the deputy district attorney."

"No. He won't want you to do that for him until after he's talked
with you a couple of times more. By that time, you'll be positive
the man was coming out of 309. Then he'll put on the demonstration
to clinch it in your mind."

She said, "He wouldn't do anything like that. He wants to be fair.
He's a very nice young man." I said, "Yeah, I know."

The waiter brought on our meat course, and after he had left, she
said, Donald, I've got to get a room.”

"Did the district attorney tell you where you were to stay?"

"No. He said to report at ten o'clock in the morning."

I said, "Look here. I want to keep in touch with you. I don't want
to have you hunting me up or running to the agency office, and I
don't want to be going to your hotel. Let's go to my rooming-house.
I'll tell the land- lady you're related to me, and ask her to give
you a room. I think she has a vacancy. In that way, I can see you
once in a while without arousing suspicion."

"Donald, I think that would be swell."

"It's not like a hotel," I said. “It's just a rooming-house, and –
“

"I know," she said.

I said, "We'll go up right after dinner. I have some work to do,
and I'll get you settled first."

“But I thought you didn't have to work. I thought Mrs. Cool said –

"She doesn't care when I work," I said, "or when I sleep. All she
wants is results. If I can get them in twenty-three hours a day,
she doesn't object to my doing anything I damn please with the
extra hour."

She laughed, then abruptly quit laughing and stared steadily at me.
"Donald," she said, "arc you working for the man who came out of
that apartment?"

I said patiently, "You don't know whether he came out of that
apartment or not, Marian."

"Well – look here, Donald. I don't want to do anything that's going
to hurt you. Don't you think it would be a swell idea for you to
put your cards on the table?"

"No."

"Why not?"

"Then you'd know too much."

"Don't you trust me?"

86

"It isn't that. You have enough troubles now, If you helped me
without knowing you were helping me, no one could make a kick. If
you helped me and knew you were helping me and it turned out I was
in hot water, then you'd be in right along with me."

She said, "Oh. Then you are working for him."

I said, "Quit talking and eat. I have work to do."

I hurried her through dinner and drove her up to my place. Mrs.
Eldridge listened to my explanation that she was my cousin who had
come to town rather unexpectedly. I said she might be there for a
day or two. I didn't know just how long.

Mrs. Eldridge gave her a front room on my floor. She looked across
at me with acid eyes and said, "When you're visiting your cousin,
leave the door open."

"I will," I said, and took the receipt Mrs. Eldridge handed me.

When she had gone, Marian said, "So we have to leave the door
open."

"Uh-huh."

"How much open?"

"Oh, two or three inches. I'm going anyway."

"Donald, I wish you didn't have to go. Can't you stay here for a
little while and – and visit."

"No. Charlie might not like it."

She screwed her face up in a little grimace and said, "I wish you'd
quit kidding about him."

"But what's his real name?" I asked.

She said, "You've created him. He is entirely your idea. If you
don't like Charlie, why don't you think up another name?"

"Charlie suits me all right."

"Then go on calling him Charlie."

I said, I've got some work to do. I've got to be shoving on.”

“Donald, I wish I could get that out of my mind. She had such a
beautiful figure, and that cord around her neck – her face was all
swollen and black and – “

"Shut up," I said. "Quit thinking about it. Go to bed and get some
sleep. The bathroom is at the end of the corridor."

"When will you be in, Donald?"

"I don't know. Pretty late."

"If I'd sit up, would you look in on me before you went to bed?"

"Why?"

"I don't want you sitting up, and it may be good and late. Go to
bed and get some sleep."

"Will you see me in the morning?"

"I can't promise."

"Why?"

"I don't know what I'll be doing in the morning."

She placed the tips of her fingers on my forearm. "Thanks for the
dinner and – and everything, Donald."

I patted her shoulder. "Keep a stiff upper lip. It'll be all right.
‘Night."

She came to the door and watched me down the corridor. Mrs.
Eldridge was waiting to buttonhole me in the front hallway. "Your
cousin looks like a nice girl," she said.

"She is."

"Of course, I like to know something about people who have rooms
here, particularly young women."

I said, "My cousin's engaged to a sailor. His boat's due to arrive
some time tomorrow."

Her nose went up in the air an inch or two. "If he calls on her,
tell her to keep the door open – or should I tell her?"

"He won't call on her," I said. "His mother lives here. She'll
visit him at his mother's. She expected to stay there, only there
was some company came in unexpectedly."

Mrs. Eldridge's face thawed into a smile. "Oh," she said, and then
after a moment: "Oh."

"Is that all?" I asked.

She said, "Under the circumstances, I won't ask you anything about
her. Usually I like to know more details, but under the
circumstances it won't be necessary for you to tell me anything at
all about her personal affairs."

"I was afraid I'd have to," I said. I went out, climbed into the
agency car, and filled it with gas, oil and water. It was darn near
empty on all three.



