## Chapter Fourteen

BERTHA COOL called on me in the receiving hospital. "I have a cab
waiting, lover, any time you feel like trying to leave. How are
you?"

The nurse looked at my chart and said, "He's suffering from a
general run-down condition as well as the shock and the gas"

Bertha said, "It's no wonder. Poor boy. He's been working
twenty-four hours a day, and he isn't built for it."

The nurse said to me, "You must take things easier." I said, "I'm
better now. I think I can leave." The nurse said, "Just a minute.
I'll get the doctor's permission."

She walked down the corridor. I heard the whir of a telephone dial,
and then she started talking, saying something in a low voice which
I couldn't understand.

I said to Bertha, "Wise me up."

Bertha, with an eye on the corridor, said, "You doped it out right.
She committed the murder."

"How about the confession?" I asked. "Did it mention Alftmont?"

Bertha said, "No. The confession was unfinished and unsigned, but
it was in her handwriting. It was one of those 'to whom it may
concern' things. It started right out by saying that she was the
one who had murdered Evaline Harris."

"Did it mention Harbet's name?"

"No. That was in the letter she wrote and addressed to me."

"Are we going to have to use that letter?"

"I don't think so."

"If we do," I said, “remember that e had left her a stamped,
addressed envelope, and told her to drop us a line about some other
matter. She mailed the letter herself and — “

Bertha Cool said, "For God's sake, Donald, don't think everybody is
dumb. I got the play as soon as you threw it over the transom. We
aren't going to have to use it. It's nice, but it's dynamite."

"She says something about Harbet in there," I said. "Spilled the
whole beans. All about how Harbet wanted to bring pressure to bear
on Dr. Alftmont."

I said, “I want to put in a telephone call for Harbet. I'll tell
him confidentially that we have — “

Bertha said, "You'll have a hell of a time reaching him. Harbet has
taken a powder. The D.A. here telephoned Santa Carlotta about the
suicide. Harbet got up from his desk, walked out, and hasn't
returned. He won't return."

I thought that over. "I wanted to be the one to tell him," I said.

"You're a vindictive little cuss, Donald."

"What did she say happened to the real Mrs. Lintig?"

"She didn't know. Amelia married Wilmen, and went down into Central
America somewhere. They never showed up again. Amelia left her
trunk with Flo. Flo kept it in her place for a while, then put it
in storage, and finally went through it and took out what she
wanted. She figured Amelia was dead."

"But she can't prove it."

"No"

I said, "That's what I was afraid of. Insist that this woman is
Amelia Lintig. Perhaps we can get by with it and get a certificate
of death."

Bertha said, "There you go again, Donald, thinking you have to
point out every play for me. For God's sake, don't you give me
credit for – The nurse came back down the corridor. A doctor was
with her. The doctor said gravely, “I'm sorry, Mr. Lam, but orders
are that as soon as you're able to leave here, you're to go to the
district attorney's office."

"You mean that I'm under arrest?"

"It amounts to that."

"For what?" I asked.

"I don't know," he said. "Those are orders. I think that you've
been under a great strain lately. You are wiry and strong.
Organically you're as sound as a nut, but your nerves can't stand
the terrific whipping you've been giving them lately. I dislike to
have you subjected to any undue strain, but those are orders. A
detective is on the way to pick you up."

I said, Can Mrs. Cool go along? I'd like to have her corroborate
parts of my story.”

"I don't think so," the doctor said. "You'll have to ask the
detective about that."

He went away. The nurse kept sticking around. After a while a
detective came in and said, "Come on, Lam. We're going to run over
to the district attorney's office."

"Who wants me over there?"

"Mr. Ellis."

I said, "What's the charge?"

"I don't know that there is any."

Bertha Cool said, "He's intensely nervous. He's in no condition to
questioned or bullied."

The detective shrugged his shoulders.

Bertha Cool took my arm and said, "I'll come right along, Donald."

The detective said, "You can go as far as the D.A.'s office. After
that, it's up to Mr. Ellis."

We went to the district attorney's office. A secretary said Mr.
Ellis wanted to see me, and Bertha Cool tagged right along. The
secretary said, "Only Mr. Lam," but Bertha couldn't hear her. Her
attitude was filled with the maternal concern of a setting hen. She
held open the door of the office marked Mr. Ellis and said, "Go
right on in, Donald," as though she'd been talking to a
five-year-old child.

I walked in. Mr. Ellis was one of these good-looking
God's-gift-to-women guys. I looked at him and could tell his story
with that one glance – a nice college boy, an athlete by the looks
of his shoulders and the bronze of his complexion, a football
player for dear old Southern California, a model student with a
high scholastic record, friends everywhere, and a habit of
ingratiating himself with his professors. They'd manipulated him
into the district attorney's office as a deputy, and he was filled
to the collar button with the abstract legal lore of a law school.

He said, "Mr. Lam, your activities in this case have been rather
remarkable"

I said, "Haven't they?"

He flushed.

I said, "It's an awful shock to learn that my own aunt is guilty of
a murder."

"And, by a remarkable coincidence," he said, "in a case which you
were investigating."

I raised my eyebrows and said, "A case I was investigating?" and
looked blankly at Bertha Cool.

Bertha Cool said, "There's some mistake. Donald is working for me.
We weren't investigating any murder."

"Why did he go to Oakview?" Ellis asked.

Bertha said, "I don't know. That was private business. He asked for
time off. It had something to do with finding his aunt. They'd been
estranged for a while, and he wanted to look her up. He found her
in Oakview, you know."

Ellis frowned and said, "Yes. I know." And then, after a moment :
“Perhaps, Mr. Lam, if you had no interest in the Evaline Harris
murder, you'll be kind enough to tell me why you took it on
yourself to run Miss Dunton into your rooming-house as your cousin,
and

"Because I thought she was in danger," I interrupted. "I formed a
friendship with Miss Dunton while I was in Oakview."

"So it would seem," he said.

I said, "I got worried about her. She told me that she could
identify a man she had seen leaving that apartment. Of course, at
the time I thought that he was the murderer – sort of took it for
granted, you know."

"That's a nice story," he said, "but I happen to know that you were
trying to keep her out of circulation. You were hiding her so we
couldn't find her."

"So you couldn't find her!" I exclaimed. “Good heavens! I don't
know – Oh, yes, I told her that I was going to notify you of her
new address. That's right. I forgot to do that. This business with
my aunt came up and – “

"What business with your aunt?" he interrupted.

I said, "She was going to marry a man who was only interested in
her money. I wanted to investigate him. I spoke to Mrs. Cool about
it, and she said that she'd use the agency and see what could be
done."

Ellis picked up a telephone and said, "Send Miss Dunton in."

A few moments later there were quick steps in the hall and Marian
Dunton opened the door. I think she expected to find us there. She
smiled and there was concern on her face. "Donald, how are you?"
she asked, and came over to give me her hand. "I heard you were at
the receiving hospital. You're white as a sheet."

I took her hand, and her left eye, the one that was farthest from
Ellis, closed in a slow, solemn wink.

She said, “You're trying to do altogether too much and you're
worrying too much, Donald. When you got worried about me, you
should have communicated with the authorities instead of taking it
on yourself to – “

"That'll do, Miss Dunton," Ellis said sternly. "I'll ask the
questions. I'd prefer that the information came from Mr. Lam."

I said, "What information do you want, Mr. Ellis?"

"How did that apartment get all mussed up?"

"What apartment?"

"The one where Miss Dunton had been staying." I said, "I wouldn't
know."

"You wouldn't know anything about the blood either?"

"Oh, yes," I said. "I know all about that. You see, I'd been having
terrific nosebleeds at intervals during the day. I went up to pack
some things for Miss Dunton and my nose started to bleed. I had a
lot of trouble with it, trying to stop it. I was afraid I was going
to have to go to a doctor. I couldn't take her things. I was
holding my nose. I left the apartment, headed for a doctor's
office, but my nose stopped bleeding before I found one."

"And you never did get back to pick up Miss Dunton's things?"

"To tell you the truth, I didn't. I started back, and came to the
conclusion someone was watching the apartment. I was afraid he
would shadow me and find out where Miss Dunton was staying."

"And you didn't push the furniture around?"

"Why, no," I said. "I don't know what you're talking about. I do
remember I fell over a chair. I was holding a handkerchief to my
face, you know."

Ellis said, “It looked as though there'd been a struggle in that
apartment. Miss Dunton's purse was lying open, and – ‘

"He told me he'd dropped my purse when he had the nosebleed,"
Marian said.

Ellis frowned, but his eyes, meeting Marian's, couldn't hold a
stern expression. He said, "Let me do it, please, Miss Dunton."

"Oh, very well," she said in a hurt voice.

Ellis couldn't get up any steam after that. He was licked. Five
minutes later, he said, "Very well. The circumstances are
exceedingly strange. After this, Mr. Lam, if you want to protect
any witness who is in communication with our office, simply advise
the office and don't take the responsibility on your own
shoulders."

I said, "I'm sorry, but I did what seemed best at the time."

I glanced at Bertha Cool, and then decided I might as well get the
whole thing straightened out while I was about it. I said to
Bertha, "What was this about some charge on a hit-and-run case
being made against me?"

She said, "Some officers were trying to pick you up at the agency
office."

Ellis said hastily, "That's quite all right. That matter has been
taken care of. You can simply ignore that. An officer at Santa
Carlotta telephoned in a short time ago. The witness who saw the
car made a mistake on the licence number."

I said to Bertha, \`Well, I guess we can go.”

Marian said, "I'm coming along, Donald, if you don't mind."

Ellis said, "Just a minute, Miss Dunton. I'd like to ask you a few
more questions, if you please – after the others leave."

Bertha Cool said, "It's all right, Marian. We'll be waiting for you
in a taxi down at the main entrance."

Walking down the corridor, I said to Bertha Cool, "Do you have that
letter Flo wrote with you?"

Bertha said, "Do I look that simple, lover? That letter's in a safe
place. How about notifying our client?"

"Too dangerous," I said. "A lot of heat has been turned on. Our
lines may be tapped. Let him read it in the papers: \`Amelia Lintig
of Oakview Confesses to Murder of Night-Club Entertainer and
Commits Suicide."

Bertha Cool said, "You've never going to get away with this aunt
business, lover. They'll nail you on that.' I said, “They're going
to have a sweet time doing it. She really was my aunt."

Bertha Cool looked at me in surprise.

"You don't know anything about my family or antecedents," I said.

"And what's more, I don't want to," Bertha hastily told me. "This
time you're on your own."

"That's swell. Just remember that."

We waited in the cab for about ten minutes, then Marian came down
looking rather flushed and elated. She flung her arms around me and
said, "Donald, it's so good to see you. Gosh, I was afraid you were
going to make the wrong play with Mr. Ellis. I'd already squared
things for you. I told him we'd formed a very close friendship, and
that you were really concerned about me."

"How did they locate you?" I asked.

"I think it's that landlady of yours," she said. "She read the
morning papers with a description of the missing witness. I don t
think she trusts you entirely, Donald."

Bertha Cool said, "I think it'll be a good idea for you to get
another rooming-house, lover."

"Mrs. Eldridge will have already arranged that," I said. And then
to Marian: "Did you have any trouble with Mr. Ellis?"

"Trouble?" Marian laughed. "Good heavens, no ! Do you know what he
wanted to ask me when he requested me to remain behind?"

Bertha Cool said, "It's an even money bet he asked you to marry
him."

Marian laughed and said, "No, not that – not yet. He's a very
conservative young man, but he did ask me to go to dinner and a
show tonight."

There was silence for a while. Marian kept looking at me as though
waiting for a question.

Bertha Cool asked it. "What did you tell him?" she asked.

Marian said, "That I had a date with Donald."

Bertha Cool sighed, and then, after a moment, said, in an
undertone, "Can me for a sardine."



