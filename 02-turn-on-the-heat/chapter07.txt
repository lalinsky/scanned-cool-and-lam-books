## Chapter Seven

IT was well after midnight when I pulled into Santa Carlotta. The
night had turned cold and I stopped in at an all-night restaurant
for a cup of hot chocolate. From a telephone in the restaurant I
rang Dr. Alftmont's residence.

The phone rang half a dozen times before a woman's voice, sounding
dopey with sleep, said, "Hello."

"Dr. Alftmont's residence?"

"Yes."

"I must speak with Dr. Alftmont at once on a matter of the greatest
importance."

"Have you tried his office?"

"His office?" I said in surprise.

"Yes. I think you'll find him there. He was called to his office
shortly before midnight and hasn't returned."

"Sorry I disturbed you," Ï said. "I hardly expected he'd be at his
office."

The woman was shaking the sleep out of her voice. She said, "That's
quite all right. I understand. Would you care to leave a message in
case you miss him at the office?"

"Tell him I'll call him in fifteen minutes if I don't catch him at
the office," I said, "and thank you very much."

"It's quite all right," she said again.

I hung up and drove to Dr. Alftmont's office. If I had been a
patient the voice of that woman would have made me a lifelong
customer.

There were lights on in the building. The elevator was on
automatic. I pressed- the button and went up to Dr. Alftmont's
floor. I couldn't hear any voices as I walked quietly down the
corridor, but the oblong of frosted glass in the door of the office
was radiating light into the corridor.

I tried the door. It was locked. I knocked a couple of times, and
then I heard a door open and close on the inside of the office. I
heard steps coming across the floor, then the door opened and Dr.
Alftmont stood staring at me. Surprise, consternation, and stark
fear took turns registering on his face. The door of the inner
office was tightly closed.

I said, “I'm sorry to bother you, Doctor, but a matter of the
greatest importance has come up and makes it necessary.' He glanced
over his shoulder at the closed door to his private office and
seemed puzzled.

I said, "Okay. We can talk right here." I took a step towards him,
lowered my voice, and said, "Do you know what happened this
afternoon?"

He hesitated a moment, then turned and said, "You may as well come
in." He walked to the door of the private office, twisted the knob,
and opened it.

I looked into the lighted interior of his testing laboratory. He
said, "Go right on through to the private office." I walked across
and opened the door.

Bertha Cool sat over in a big chair near the window. She looked up
at me, and her face showed surprise. I said, "You!"

Dr. Alftmont came in behind me and closed the door. Bertha said,
"Well, well, Donald. You do get around, don't you?"

"How long have you been here?" I asked.

"About an hour," she said.

Dr. Alftmont crossed over to sit behind his desk. "This is
terrible," he said, "terrible!"

I kept my eyes on Bertha. "How much did you tell him?" I asked.

"I explained the situation to him."

I said, "All right. Just a minute," and walked around the office,
looking in behind the pictures and bookcase, moving the framed
pictures out from the wall.

Dr. Alftmont said, “What are you loo – “

I held up a warning finger to my lips, and motioned to the wall.

Bertha Cool got the idea and said, "For God's sake, Donald!"

I didn't say anything until I had completed a search of the office.
I said, "I don't see any. Which doesn't mean there isn't any. You
have to be careful, particularly of that." I pointed towards the
telephone.

Dr. Alftmont started to get up, then sat down again. He seemed
completely overwhelmed by the turn of events. I said to Bertha,
"Have you concluded your business?"

"Yes," she said, and then added, with a smile, "quite
satisfactorily so far as we're concerned, Donald."

"Finished with everything you had to say?"

"Yes," she said.

"All right," I told her. "Let's go."

Dr. Alftmont said, "I'm afraid I don't quite understand."

I'll be back in about ten minutes, Doctor,” I said. "Would you mind
waiting?"

"Why - no, I guess not."

I nodded to Bertha. She looked at me rather peculiarly, heaved to
her feet, and gave Dr. Alftmont her hand. "Don't worry," she said.
"11 11 be all right."

"I wish I could share your confidence."

"It's all right. You're in our hands. We'll take care of you."

I said, "Wait fifteen minutes," to Dr. Alftmont and walked down the
corridor with Bertha Cool. Neither of us said anything in the
corridor. In the elevator I said, "How did you come up?"

"I hired a car with a driver."

I said, "We'll talk in the agency car. It's downstairs"

We walked out across the strip of dark sidewalk, and Bertha Cool
sagged the car over on its noisy springs as she eased herself into
the pile of junk. I started the motor, drove down a couple of
blocks, and parked in front of an all-night restaurant where we
wouldn't attract so much attention. "What did you tell him?" I
asked.

"Enough to let him know that we control the situation."

I said, "Where did you leave your car?"

"In the middle of the next block," she said. "The driver's waiting.
I told him not to wait in front of the office." I started the motor
on the office heap.

"Didn't you want to talk, Donald?" she asked.

"There's nothing to talk about now," I said. "The beans are
spilled."

"What do you mean?"

"I was going to tell him that a witness had seen a man leaving the
apartment. I wasn't going to let on that we had any idea of who it
could have been. His conscience would have done that."

"If he knows, why shouldn't we let him know that we know?"

"Just a legal difference," I said. "If we were helping him without
any idea that he was the.. one, we'd have been acting as
detectives. He naturally wouldn't haveclimbed out on a limb and
told us anything. I suppose now you know it all."

"Yes," she said. "He went down to see her. He wanted to find out
who sent her and what she'd discovered and see if he could make a
deal with her."

"She was dead when he found her?" I asked. "That's what he says."

"All right," I said to Bertha, "here's your car. Better drive back.
I have a breakfast date at seven-thirty in the morning. I don't
think I'll keep it. She's in my rooming-house, number thirty-two.
Take her to breakfast with you. Stall her along. Get her to give up
the room. You'd better get her an apartment somewhere. The D.A.
will want to know where she's staying. The way things are now, it
had better be away from my joint."

For a moment the self-sufficiency oozed out of Bertha Cool's
manner. She said, in something that was almost' a panic, "Donald,
you'll have to come back with me. You must. I can't control that
girl. She's fallen for you. She'll do anything on earth you tell
her, and I can't - God, Donald, I didn't know I was laying myself
wide open that way."

"You see the point, don't you?"

"I see it now," she said.

"I have work to do here."

"What?"

I shook my head, and said, "There's no use explaining to you. The
more you know, the more you talk. The more you talk, the more you
put us in a position of being accessories after the fact. I'd have
done a damn sight better if I'd held out on you from the start. I
tried to, but you insisted on horning in."

She said, "He's wealthy, Donald. I got a cheque for three thousand
dollars."

"I don't care if you got a cheque for ten thousand," I said.
"You're in a jam. If there was a dictograph in that office, you're
sunk. Bring your conversation with him up in front of a grand jury,
and you can figure how long it'll be before your licence gets
revoked. After that, you'll be in prison. You won't take me with
you - in case that's any consolation to you."

I could see she was frightened. She said, "Donald, come on back
with me. There's nothing you can do here tonight. Leave the agency
car. You can drive back with me. It's a warm, comfortable closed
car. You take Marian to breakfast in the morning and get her a
nice, quiet apartment somewhere."

I said, "No. Get her both an apartment somewhere and a room in a
hotel. She goes to the hotel room once a day to pick up mail and
messages. The rest of the time she stays in the apartment."

"Why?" Bertha asked.

I said, "She can't be too accessible. You can figure the play. They
have organized vice and organized graft in this city. Alftmont
can't be bribed. He's running for mayor. If he's elected, he'll
start cleaning up the city. Lots of people don't like that. Some of
them are on the police force. They can dig up this scandal and play
it either of two ways – to keep him from being elected or make him
withdraw from the race, or they can let him be elected and hold it
as a club over his head. They've been working quietly on it for a
couple of months. Then he walks right into the middle of a murder.
He couldn't afford to notify the police because the newspapers
would start asking questions about why he'd gone to the apartment
of a night-spot commission girl. He'd figure her trip to Oakview
would be dragged out. He knew the local police would try to frame
the crime on him, and he had to make a sneak. It just happened he
ran into Marian in the hall. That was his hard luck. Our business
now is to keep the Homicide Squad from suspecting the case has a
tie-in with Santa Carlotta, to keep Marian Dunton from ever seeing
Dr. Alftmont."

"That shouldn't be hard," she said.

I laughed. ,”Remember the man who beat me up and kicked me out of
Oakview?” I asked.

"What about him?" she asked.

I said, "His name is John Harbet. He was Evaline Harris's
particular boy friend. He has a tie-in with the man who runs the
Blue Cave. He's the head of the Vice Squad in Santa Carlotta.
Figure that out."

While she was studying that bit of information, I opened the door
of the agency car and said, "Okay, there's your bus. Get started,
and don't forget to be on hand to take Marian out for breakfast.
And just one other thing. I told that girl to act dumb. She's doing
it because she knows it's the thing to do, but don't kid yourself.
She's country, but she isn't dumb. And she's a darn nice kid."

Bertha Cool put her left hand on my right arm. "Lister, lover, come
hack with me. Bertha needs you."

I said, "Any minute now, a cop may come along the street and turn a
flashlight on us just to see who we are. Would you like that?"

Bertha Cool said, "Hell, no!"

She scrambled out of the car as though it had been on fire. The
driver of her car unwrapped himself from behind the steering-wheel
and stood holding the door open for her. She gave me one last
appealing look, then climbed into the closed car. She sank back
against the cushions, and for the moment she didn't look big and
hard and competent. She looked like a fat woman in the fifties who
was tired out.

I drove around the block, parked the agency car across the street
from Dr. Alftmont's office, and went up. He was waiting for me.

I said, "You know too much, and we know too much. Bertha talked too
much. I want to talk with you, and. I don't want to talk with you
here. Let's take a little ride in your car."

Without a word he switched out the lights, locked up his office,
and rode down in the elevator with me. His car was parked at the
kerb in front of the building entrance. "Just where do we go?" he
asked in that precise voice of his.

"Some place where we can talk, and where we won't be seen," I
said.

He was nervous. "They have a police radio car that investigates
parked automobiles."

"Don't park then."

"I can't talk when I'm driving."

"How about your house?" I asked.

He said, "We could talk there."

"Let's go – if it won't inconvenience your wife."

"No, no. It's all right. We can go there." There was relief in his
voice.

"Does your wife know anything about the jam you're in?" I asked.

"She knows all about it."

I said, "Don't think I'm taking liberties with your personal
affairs, but is your wife's first name Vivian?" He said, “Yes “

No one said anything after that. He drove the car up the main
street, turned to the left, climbed a hill, and entered a
high-class residential section with modem houses of Spanish-type
architecture — white stucco sides and red tile roofs showing to
advantage against the dark green of shrubbery — a green which was
almost black in the spaces between street lights.

We turned into the driveway and rolled into the garage of a
pretentious stucco structure. Dr. Alftmont switched off the
headlights and the ignition, and said, "Well, we're here."

I got out of the car. Dr. Alftmont led the way towards a door which
opened on a flight of stairs, then opened another door, and we
entered a hallway. The woman's voice I'd heard over the telephone
said, "Is that you, Charles?"

"Yes," he said. "I. have someone with me."

She said, “A man telephoned and —

"I know. He's here with me," Dr. Alftmont said. "Won't you come in
this way, Mr. Lam?"

He ushered me into a living-room. The furniture was expensive but
quiet. The drapes, carpet, and decorations all harmonized in a
quiet blend of colour.

The woman's voice said,, "Charles, let me talk with you a moment,
please."

Dr. Alftmont said to me, "Excuse me a moment," and went back down
the corridor towards the stairs. I heard low-voiced conversation.
It kept up for four or five minutes. Then I heard her asking
something of Dr. Alftmont. She kept pleading. He made short answers
in a voice which sounded like courteous but firm negatives.

Steps coming down the corridor again; this time there were two
people approaching. I got up out of my chair as the woman entered
the room. Dr. Alftmont, a step behind her, said, "Dear, may I
present Mr. Lam. Mr. Lam, this is Mrs. Alftmont."

The accent on the "Mrs." was belligerent.

She had kept her figure remarkably well. She was somewhere in the
forties, but she moved with easy grace. The hazel eyes were steady
and frank. I bowed and said, "Pleased to meet you, Mrs. Alftmont."

She came towards me and gave me her hand. She'd put on a dark blue
dress which harmonized with her colouring and set off her figure.
Something about my telephone call had made her get up and dress.
I'd have gambled she was in bed when I'd called.

She said, "Won't you sit down, Mr. Lam?"

I sat down. She and Doc Alftmont took chairs. Alftmont seemed
nervous.

Mrs. Afltmont said, "I understand you're a detective, Mr. Lam."

"That's right."

Her voice was well modulated and seemed to come without effort.
There was no evidence of strain anywhere about her. Doc Alftmont
gave the impression of weighing words with meticulous care lest he
betray himself in a moment of inadvertence. She radiated the quiet
poise and the calmness of a woman who has never tried to kid
herself.

She said to her husband, "Give me a cigarette, Charles," and then
to me, "You don't need to mince words, Mr. Lam. I know all about
it."

I said, "All right. Let's talk."

Dr. Alftmont handed her a cigarette and held a match. "Want one,
Lam?" he asked.

I nodded.

Dr. Alftmont shook out the match, handed me a cigarette, took one
himself, and we both lit off the same match. He turned to her and
said, "Mrs. Cool was at my office, dear. Mr.. Lam didn't come with
her. He carre—"

"On my own," I interrupted.

Dr. Alftmont nodded.

The woman had kept her eyes on me in steady appraisal. She said,
"Go ahead, Mr. Lam."

I said to Doc Alftmont, "I presume Bertha Cool did most of the
talking."

He nodded.

I said, "Bertha wanted to convince you you were in a jam, so you'd
kick through with some more money. Is that right?"

"Well," he said, after a moment, "it amounted to that."

"All right," I said. "That's her end of it. That's taken care of.
My end is to do the actual work. It's up to me to get you out of
this jam. I want you to talk with me."

"What," he asked, "do you want to talk about?"

"I want to know what you're up against, and I want to know what I'm
up against."

He glanced at his wife.

She said, “I'm Vivian Carter. We have no children.

We're not married legally, although a ceremony was performed in
Mexico about ten years ago.”

I said to Alftmont, "Tell me about that divorce case."

"What about it?"

"All about it," I said.

He placed the tips of his fingers together and said, “To begin
with, my first wife, Mrs. Lintig, was swept into the hectic swirl
of social change which came with the war. There was an emotional
backwash which resulted in a breakdown of the conventions. There
were – “

I held up my hand, palm outward, giving him a traffic officer's
stop signal, and said to the woman, \`Suppose you tell me.”

She said, easily and naturally, "I was an office nurse for Dr.
Lintig. I fell in love with him. He didn't know anything about it.
I made up my mind he'd never know. I was perfectly willing to let
Amelia – Mrs. Lintig – have the position of wife and the affection
of her husband. I asked only crumbs – the chance to be near him,
and I kept very much in the background"

Dr. Alftmont nodded vigorously.

“I wanted to serve him, to be where I could help. I was young and
foolish. I know the answer to that one now, but I didn't twenty-one
years ago. Oakview was in the throes of a boom. New people were
coming in. Money was plentiful. There was, as Charles has said, a
period of hectic change. Amelia went for it in a big way. She
started drinking heavily and became a leader of the younger set.
The standards of that social set were different from anything which
had ever been known before. There was drinking, petting, and – and
brawling. Charles didn't like it. Amelia did.

"Amelia started to play around. The doctor didn't know that, but he
was fed up. He told her he wanted a divorce. She agreed and asked
him to get it on grounds of mental cruelty. He filed suit. Amelia
didn't play fair. She never did. She waited until I went to San
Francisco on some business for the doctor, and then filed a
cross-complaint naming me as co-respondent, apparently because she
thought by beating him to the punch and hitting at me she could get
all the doctor's property, and marry the man with whom she was
infatuated at the moment."

"Who was that?" I asked. Her glance asked permission of Dr.
Alftmont.

He nodded. She said, "Steve Dunton, a young chap who was editing
the Oakview Blade."

I held expression from my face and asked, "Does he run it now?"

I think so, yes. We've pretty much lost track of Oak-view, but I
believe he's still there. His niece was working with him on the
paper the last I heard.”

Dr. Alftmont said. "That was the niece who met me in the corridor
of the apartment house, you know."

I dropped ashes from my cigarette into an -ash-tray, and said, "Go
ahead."

"At that time," Mrs. Alftmont said, with just a trace of bitterness
in her voice, “there had never been the slightest indiscretion, and
Charles had no idea of how I felt towards him. I think Amelia was
hardly herself. Her . temperament, her irrational mode of living,
and the liquor she was drinking made her exceedingly erratic.

"When she filed that cross-complaint, Charles rushed to San
Francisco to explain things. I saw right away that he was in an
awful spot. Oakview would seethe with gossip. The person who was
really the most interested in Mrs. Lintig's divorce was employed on
the paper. He was going to see that any circumstances which could
be tortured into evidence against Charles were given plenty of
publicity. His trip to San Francisco was, of course, the worst move
he could have made. At that, we would have returned and fought
things out if it hadn't been –" She became silent.

Dr. Alftmont said simply, “I made a discovery. As Amelia had
developed those tendencies which were so distasteful to me, I had
been gradually falling out of love with her and in love with
Vivian. I made the discovery when I met Vivian in San Francisco.
After that, I couldn't go back, drag her name through the mud, and
– well, we knew we loved each other then. We only wanted to be
together. We were young. I wanted to go away and begin all over
again. Probably it was foolish of me, although as events have
turned out, it was for the best.

"I telephoned Amelia and asked her what she wanted. Her terms were
simple. She wanted everything. She'd give me my freedom. I could
clear out and begin all over again. I had some travellers' cheques,
several thousand dollars. She didn't know about those. I'd been
afraid of the boom activities of Oakview and had distrusted the
bank."

"Then what?" I asked.

He said, "That's virtually all there is to it. I took her at her
word. She said she'd go ahead and get the divorce, that I could
change my name and start practising somewhere else, that as soon as
the decree was final, I could marry Vivian. I accepted her terms."

"Do you know exactly what happened?" I asked.

"No, he said. “I understand Amelia and Steve Dun-ton had a falling
out. I don't know. She left Oakview and vanished."

"Why didn't you quietly sue for a divorce somewhere else?" I
asked.

"She found me," he said. "I received a letter from her stating that
she would never let me cast a mantle of respectability over Vivian,
that if I ever tried to marry Vivian she would appear and make
trouble, that if I ever filed a divorce suit she'd expose the whole
thing – by that time, living with Vivian as man and wife, she could
have made a perfect case – and a scandal."

"She knew where you were?"

"Yes."

"Why didn't you go ahead anyway?"

"I couldn't," he said. “In the year that we had been living here as
husband and wife. I had built up a fairly good practice among the
respectable, conservative people. If it had come out that Vivian
and I were living together without benefit of clergy, it would have
been fatal.'

"Then what?" I asked.

"Years passed," he said. "We heard nothing more. I tried to trace
her and couldn't. I felt certain that she'd either died or divorced
me and remarried. Some ten years ago, Vivian and I quietly slipped
across to Mexico and were married. I thought the ceremony would
give her a legal standing in the event it should become
necessary."

"All right," I said. "Tell me about the political angle."

Dr. Alftmont said, "This city is standing in its own light. Our
police force is corrupt. The city administration is honeycombed
with graft. We have a wealthy city, good business, and a fine
tourist trade.. Those tourists are forced to nib elbows with every
form of racket. The citizens were tired of it. They wanted a
clean-up. I had been instrumental in getting some of the citizens
organized. They insisted I should run for mayor. I thought that old
scandal was dead and buried, so I agreed to run."

"And then what?"

“Then, out of a clear sky, I received a letter from her, stating
that until I made terms with her, I could never be elected, that at
the last minute she would, as she expressed it, blow the lid off.
She charged that I had cast her into the junk pile and left her a
social and financial outcast – although I had done nothing of the
sort. I had stripped myself of my property, and – “

Charles, Mrs. Alftmont interrupted, "that isn't going to help any
now.. Mr. Lam wants the facts."

"The facts," he said, "were that she wrote this letter."

"What were her terms?" I asked.

"She didn't offer any."

I did some thinking over the last puffs of my cigarette, then
ground it out, and asked, "Did she give you any address where you
could get in touch with her?"

"No."

"What did she want?"

First, she wanted me to withdraw from the campaign.”

"You didn't do that?"

"No."

"Why?"

"I was in too deep," he said. "Shortly before that letter was
received, the opposition newspaper started publishing a series of
slurring articles, intimating that my past would stand
investigation. My friends demanded that I sue the paper for libel.
I was placed in a very embarrassing position."

"Are you," I asked, "absolutely certain that the letter you
received was in your wife's handwriting?"

"Yes," he said. "There are, of course, certain changes which are
only natural. A person's handwriting has a tendency to change
during a period of twenty-odd years, but there can be no question.
I have compared the handwriting carefully"

"Where are the letters?" I asked.

"I have them," he said.

"I want them."

He glanced at his wife. She nodded. He got up and said, "It will
take a few minutes. If you'll excuse me, please."

I heard hls feet slowly climbing the stairs. I turned to Mrs.
Alftmont. She was staring steadily at me.

"What can you do?" she asked.

"I don't know," I said. "We'll do all we can."

"That may not be enough."

"It may not," I admitted.

"Would it help," she asked quietly, "if I should step out of the
picture – if I should disappear?"

I thought that over for a while, and said, "No. It wouldn't help."

"Stay and take it on the chin?" she asked.

"Yes.' She said, “I don't care a thing in the world so far as I'm
concerned, but it makes an enormous difference to Charles."

"I know it does."

"Of course," she said, “if the true facts were known, I think
public sentiment – “

"Forget it," I said. "It isn't a question of public sentiment now.
It isn't a question of scandal. It isn't a question of extramarital
relations. He's facing a murder charge."

"I see," she said, without batting an eyelash.

I said, "I think Evaline Harris was sent to Oakview by a man named
John Harbet."

Her eyes were veiled, without expression. "You mean Sergeant Harbet
of the Vice Squad?"

"Yes."

"What makes you think so?"

"He was in Oakview. He beat me up and dragged me out of town."

"Why?"

"That," I said, "is the thing I can't figure. When I find out why
he did what he did in the way he did, I think I'll have a weapon we
can use."

She frowned thoughtfully. "It's very hard on Charles. He's almost
frantic. He suppresses himself behind a mask of professional calm.
I'm getting afraid of what may happen."

I said, "Don't worry about it. Leave that to me."

Steps on the stairs again, and Dr. Alftmont came into the room with
two letters. One of them was dated 192I and was written on the
stationery of the Bickmere Hotel in San Francisco. The other letter
had been written two weeks before, and mailed from Los Angeles.
Apparently both were in the same handwriting.

I said, "Did you try to reach her at the Bickmere Hotel, Doctor?"

"Yes," he said. "I wrote her a letter. It was returned with a
statement that no such party was registered there." I studied the
letter for a while, and then said, "What was her maiden name?"

"Sellar. Amelia Rosa Sellar."

"Did she have any parents living?"

"No, no relatives. An aunt back East had raised her, but the aunt
died when she was seventeen. She's been on her own since then."

“I presume you didn't try very hard to locate her when this first
letter was written?'

"I didn't employ detectives," he said, "if that's what you mean. I
wrote to her at the hotel. When, my letter was returned, I took it
for granted she'd simply used the hotel stationery as a blind."

"At that time," I said, "she wasn't trying to keep under cover. She
had the whip hand and knew it. She wasn't trying to get property
then. She was simply trying to keep you from making Vivian Carter
Mrs. Alftmont."

"Then why didn't she let me know where I could reach her?" he
asked.

I thought that over for a minute and said, "Because she was doing
something she didn't want you to know about, something that would
have given you the whip hand if you'd found out about it. That's
where we'll start our investigation."

I caught a note of quick hope in Mrs. Alftmont's voice. She said,
"Charles, I believe he s right."

Alftmont said, “I can believe anything of her. She became selfish,
neurotic. Her ego demanded flattery. She was never happy unless
some man was paying attention to her. She wanted to be on the go
all the time. She was apparently trying to escape any form of
routine, any conventional –

"I know the type," I said. "Never mind putting it in medical
terms."

"She is selfish, tricky, deceitful, and unbalanced," he said. "You
can expect anything of her. Once she starts, she won't stop
anywhere."

I got to my feet and said, "I'm taking these letters. Is there a
night train through here for San Francisco?"

"None now," he said.

"How about a bus?" I asked.

"I think there's a bus goes through."

"I've had about all the night driving I want for a while," I said.
"I'm taking these letters with me."

"You'll take good care of them?" he asked.

I nodded.

Mrs. Alftmont walked across to give me the pressure of firm, strong
fingers on my hand. "You've brought disturbing news," she said,
"yet I feel reassured. I want to protect Charles. As far as I'm
concerned, I have no regrets. A true, deep love is all the marriage
a woman needs. I have always felt married to Charles. If there's to
be a scandal, we have each other. As for the murder – you'll have
to handle that, Mr. Lam."

"Yes," I said, "I'll have to handle that."



