## Chapter Twelve

BERTHA showed up in a taxi about nine-thirty. I thought she looked
plenty worried. She came over and told the operative, "There'll he
a relief for you in half an hour. Give me a ring shortly before
five and I'll let you know whether you work tonight."

He said, "Thanks."

Bertha said, "You can go wash your hands while we're in there. She
won't leave while we're there."

The operative said, "Thanks," and added with a grin, "My hands are
clean. Lam held the fort for a while early this morning."

Bertha looked me over and said, "Donald, you look hell."

I didn't bother to say anything.

Bertha said, "Drive around to the alley and tell the operative
who's watching the back that I'll have a relief for him. Tell him
to call up shortly before five. You can leave the agency car out in
front."

She looked at me. "Okay, lover?" she asked.

"Okay," I said. "What's new?"

We started across the street towards the entrance of the apartment
house. She avoided my eyes. "Come on," I said. "Let's have it.
What's new?"

"A telegram from the Bureau of Vital Statistics."

"Saying what?"

"Amelia Sellar married John Wilmen in February of 1922. She was
never divorced. There's no record of the death of either Amelia or
John Wilmen. Where does that leave us, Donald?"

"Right in front of the Key West Apartments," I said, "with a tough
job on our hands."

"What are we going to say to her?"

“It'll depend on how she reacts. You let me take the lead. Then you
follow my play. I've been doing a lot of thinking. Today is
probably the time they intend to spring their trap. There's just
time enough before election to let the news get exaggerated by
word-of-mouth gossip. There isn't time for any refutation.

"Had breakfast?" Bertha asked.

"Yes."

The day clerk at the desk smiled at us. I nodded and walked past
him to the switchboard. Frieda Tarbing looked up with a perfectly
blank face.

"Will you ring Mrs. Lintig," I said, "and tell her that her dutiful
nephew is in the lobby. Please ring very quietly because if she's
asleep I don't want to disturb her."

I saw a quick flicker of comprehension on Frieda Tarbing's face.
"Ring quietly?" she asked.

"Very quietly," I said.

"I get you," she said.

The clerk gave us the once-over then turned away. Frieda Tarbing
went through motions at the switchboard and said, out of the corner
of her mouth, "Do you really want me to ring?"

"No," I said.

She raised her voice, and said, "Mrs. Lintig says for you to go
right up. It's forty-three A on the fourth floor."

I thanked her, and Bertha Cool and I walked into the elevator. A
coloured elevator boy shot us up to the fourth floor. The Key West
was an apartment that had just a touch of swank. The service was
quietly efficient.

We walked down to 43A, and I knocked on the door.

Almost immediately we heard motion on the other side of the door,
and I said to Bertha Cool, "Today's the day all right. She's up and
ready. Probably she's due to drive up to Santa Carlotta and be
there by afternoon. They'll let the story break this evening."

The door opened then. The woman I'd seen in Oak-view stood on the
threshold. She stared at me frowning, then suddenly recognition
dawned on her face. I noticed that she wasn't wearing spectacles.

"Good morning, Mrs. Lintig," I exclaimed cordially. "You'll
remember me. I'm from the Blade in Oakview. A friend of yours,
Sergeant Harbet, told me he thought you'd have a story ready for
me."

She frowned and said, "I didn't know he wanted it published in
Oakview. I didn't — Do you know Sergeant Harbet?"

"Sure," I said. "We're old buddies."

She said dubiously, "Well, come in."

I said, "This is Bertha Cool, Mrs. Lintig."

Bertha Cool flashed her diamonds, and Mrs. Lintig smiled all over
her face. "So pleased to meet you, Mrs. Cool. Won't you come in?"

We went in. I closed the door and noticed there was a spring lock
that clicked into position. I said, "I don't know the details. I
understand the Santa Carlotta paper is to publish it the same time
we do."

"And who was it sent you?" she asked.

"Why, John," I said. "John Harbet. He said you knew all about it."

"Oh, yes," she said. "You'll excuse me if I'm a little cautious.
Here's the story. I think you know the first part of it, how my
husband ran away and left me absolutely destitute."

"Didn't you get some property?" I asked.

She snapped her fingers and said, "A mere sop! I didn't get enough
money out of it to keep me for two years. It's been twenty-one
years since he ran away with that hussy. I've been searching for
him, trying to find him. The other day I located him, and where do
you think he was?"

"Santa Carlotta?" I asked.

She said, "Is that a good guess or did John tell you?"

"It's more than a good guess," I said.

"Well, he's in Santa Carlotta, all right, under the name of Dr.
Charles Loring Alftmont. He's living shamelessly and openly with
that Carter girl, and they have the crust to pose in the community
as man and wife, but the most startling thing of all is he's
running for mayor. Can you imagine that?"

I gave a low whistle.

She said, "Now, I don't want to be vindictive, but I certainly am
not going to have this creature dropping a mantle of respectability
over her scarlet shoulders, and then adding insult to injury by
becoming the Mrs. Mayor of Santa Carlotta. I think my husband will
withdraw from the campaign on the eve of election. If he does, you
understand the story isn't to be published."

I said, "I understand. John told me all about that. I promised to
hold it until I got a release."

She said, "Of course, you can play up the local angle."

I said, "That's fine. That'll make a nice story. Now, about this
Evaline Harris who came up to Oakview, and was subsequently
murdered. I understand she was doing some work for you, trying to
find out about your husband."

The woman's face became a cold mask of suspicion. "John didn't tell
you that," she said.

"Why, yes," I said. "That is, not in so many words, but he dropped
some remarks which led me to believe that was the case."

She said, "What did you say your name was? I've forgotten."

"Lam," I said. "Donald Lam."

She said, with growing suspicion in her eyes, "John never mentioned
to me that he had a friend on the newspaper in Oakview."

I laughed and said, "He didn't know where I was until just the
other day. I've known John for years."

She reached a decision and said, "Well, John certainly didn't tell
you anything about that Harris girl because he didn't know anything
to tell. I never saw her in my life."

"You're certain of that??" I asked.

"Yes, yes," she said. "Why not?"

I said, "That's funny. Because she worked as an entertainer at the
Blue Cave, and you were employed there as hostess."

She caught her breath.

I said, "I'm trying to get this straight for our paper. I don't
want to make any mistake and publish something that doesn't
click."

Her eyes narrowed. She said, "You're lying to me. You don't know
John Harbet."

I laughed easily and said, "Any time you think I don't! John and I
are just like that." I held up two fingers.

In a low, hoarse voice she said, "You get out of here! Both of
you!"

I drew up a chair and sat down, nodded to Bertha Cool, and said,
"Have a chair."

The woman said, "I said for you to get out of here." I said, "Sit
down and keep your shirt on. We're going to ask you some
questions."

“Who are you P ' she asked.

I said, "We're detectives."

She sat down as though the strength had oozed out of her knees. She
looked at me with a face that was filled with despair.

I said, "It's been rather a long, tedious trail, Flo, but we've
unravelled most of it. You roomed with Amelia up in San Francisco.
You found out all about her life history, and after she married
Wilmen, you got possession of her papers, probably out of a trunk
she'd left with you, or you may have stolen them. Anyway, you got
them."

"That's a lie," she said.

I said, “Recently, the political ring that was controlling Santa
Carlotta wanted to find Mrs. Lintig. There was money in it. You
were approached. You couldn't find Amelia Lintig; perhaps because
she's dead, perhaps because she's moved out of the state. But you
convinced them you could do a good job of impersonation. You knew
all about her background.

"You had certain things on which you wanted to check. You were
pretty close to Evaline Harris who was working in the night spot
where you were hostess. You arranged to send her up to Oakview and
have her make the investigations. Particularly you wanted her to
pick up all of the photographs of Amelia Lintig that could be
found."

"You're absolutely crazy," she said.

I said, “Now we go on from there. Evaline Harris came back with the
photographs all right, but she also had an overpowering curiosrty.
She was a chiseller, and she was greedy. Her trunk had been smashed
in shipment. She knew you'd never consent to having her make a
claim for damages because you didn't want any- one to trace her,
but, without consulting you, she went ahead and made the claim. You
found out she'd been traced. That caused a lot of trouble.

“John Harbet was giving you instructions. You were going to him for
advice. He knew all about Evaline Harris. When he first started
looking for Amelia, the trail led to you. While he was giving you
the once-over, he hung around the Blue Cave. He was friendly with
Evaline. He worked with her, coaching her and giving her
instructions on what she was to do in Oakview.

She said, in a dull, mechanical voice, "That's a lie."

"No, it isn't a lie. It's the truth. It can be proved. Now then,
when Evaline Harris left that back trail by putting in a claim with
the railroad company for her damaged trunk, Harbet blew up. That
was when Evaline Harris tried to cut herself in on the deal. She
wanted some coin to keep from talking – and that's why she was
found strangled in bed. Now then, Flo Danzer, it's your move."

She came towards me. “Damn you, get out of here, or I'll claw your
eyes out. I'll scratch your face. I'll – “

Bertha Cool's big arm swung around like a pile driver. She caught a
fistful of Flo's hair, jerked her head back, and said, "Shut up, or
I'll knock your teeth down your throat. Sit down in that chair and
stay there. That's better."

Bertha Cool relaxed her hold on the woman's hair.

For a moment they glowered at each other, Bertha Cool towering over
the woman in the chair. Then Bertha said, "I can be just as tough
as you are. You've had a background which gives you a strong
stomach, but you haven't seen anything yet. I'm really hard."

Flo Danzer said, It's a damn lie, but it makes a good story. I
suppose it's a shakedown. What do you want?”

Bertha Cool said, “Don't go near Santa Carlotta. Don't have
anything – “

"Wait a minute," I interrupted. "That Santa Carlotta business is
out anyhow. We'd show her up for an impostor within five minutes
after she made the claim. What we want right now is to clean up
this murder."

"What do you want out of me?" she asked.

"I want the facts on the Harris murder," I said. "I want everything
you know."

She started to laugh then, and I could see hard defiance in her
eyes. "Well, go jump in the lake," she said. "You've run a damn
good bluff, and it's got you nowhere. You win on one thing. I'm not
going to stick my neck out in Santa Carlotta. John Harbet will just
have to get along without me. As far as the rest of it is
concerned, you're barking up the wrong tree, and if you don't think
I know what I m doing, just stick around and I'll call the cops."

"A fat chance of you calling the cops," I said.

She said, “That shows all you know about it. If you'd waited until
this afternoon when I'd driven to Santa Carlotta and given my
statement to the Courier, told them I had come for a settlement
with Dr. Alftmont, and then disappeared, you'd have had something
you could pin on me, and – “

"You were going to disappear?" I asked.

Her laugh was a sneer. She said, “Of course I was. For a smart
Lick, you're awfully dumb about some things. I couldn't let
Alftmont lamp me. He'd know I wasn't Amelia as soon as he saw me. I
was going to tell my story to a newspaper reporter. I was going to
say that I had an appointment with Dr. Alftmont. Then I was going
to disappear. It was going to look as though I'd been bumped off,
and the evidence was going to point to Alftmont. About the time he
was denying that, we were going to connect him up with Evaline
Harris, and the police down here were going to accuse him of the
Harris murder. The witness would identify him, and that would have
been all there was to it. Public opinion would have been divided
over whether he'd killed me or not, but when they added the Harris
business on top of it, he wouldn't have stood a chance.

"Now then, that's all there is to it. Alftmont murdered Evaline. I
hope they hang a first degree on him for that. He tried to get some
information out of her, and she wouldn't kick through. The party
got rough. Don't kid yourself about Dr. Charles Loring Alftmont.
He's a killer. I'm no tin angel myself, but I can't stomach murder.
If you'd waited until this afternoon, you could have pinched me for
something. As it is, I'm in the clear. You can't do a damn thing.
If you don't get out of here, I'll call the cops."

I said, "When did you last see Evaline Harris alive?" She said,
"About twenty-four hours before she was murdered. I told her to
watch out for Alftmont."

"Why?"

"Because I knew he was dangerous."

"Then you knew that Alftmont could find her?"

She squinted her eyes. "I knew some detectives were working on the
case. I found out that Evaline had been a greedy little bitch, and
couldn't resist the temptation of trying to pick up a piece of
change from the railroad company. That was the worst of Evaline.
You could never trust her. Lots of the girls in her racket pick up
steady boy friends who make regular ‘donations – not Evaline. She
was too greedy, and she couldn't resist blackmail. As soon as she'd
get her hooks into somenice young chap, she'd find out all about
him, and then start blackmailing. You couldn't control her for a
minute. She couldn't control herself. It was like dope. She wanted
to chisel."

I said, “When the police found her body in the apartment, they
found she'd been on an all-night party and was sleeping late. The
newspaper was under the door. That means she hadn't got up. There
were cigarette stubs and an ash tray by the bed. One of them had
lipstick on it. One of them didn t.

“Evaline slept with a package of cigarettes and matches by the bed.
She always had a cigarette first thing after she woke up. I know
that.

“Now then, I figure someone went to see Evaline. It was someone she
knew. She sat down on the bed, and they talked. The talk didn't go
to suit this man, and he slipped a loop over her neck – and I think
you know who that man was.

"Sure, I do," she said. "It was Dr. Alftmont. He'd traced her –
probably through that claim she'd made to the railroad company. He
went down to see her. He was willing to be reasonable with her, but
he found out she was just a tool, that there were bigger game
afoot. He couldn't buy her off, so he croaked her. Now then, you
can either get the hell out of here, or I'll call the cops, and I
mean it."

I said to Bertha, with a surreptitious wink, "Well, the police are
working on that package of cigarettes and on the cigarette stubs,
using that new iodine process for developing fingerprints. Don't
kid yourself. They're going to get the prints of the man who called
on Evaline. Won't it be too bad if those are the prints of Sergeant
Harbet of the Santa Carlotta police force, and won't it be funny if
Harbet drags Flo Danzer into the picture."

"Don't be silly," Flo Danzer said, "How's he going to drag me into
the picture? I'll stand up on my two feet and admit everything I
did – I went to Oakview and said I was Mrs. Lintig – so what? Maybe
I intended to work some blackmail on Dr. Lintig. Maybe I didn't. I
haven't asked anyone for five cents in cash. And don't kid yourself
John Harbet is ever going to get dragged into this thing. Dr.
Alftmont is the one who's holding the bag on this rap. He lost his
head and killed Evaline."

I nodded to Bertha, got up, and started over towards the door.

"Come on, Bertha," I said.

She hesitated.

"Come on. We're going down to the district attorney's office and
put the cards on the table. We're going to get a warrant for Flo
Danzer and John Harbet on criminal conspiracy. We can prove the
conspiracy, and her going to Oakview and registering as Mrs. Lintig
was an overt act. She isn't in the clear. She only thinks she's in
the clear."

Bertha said, “Now listen. I — “

I raised my voice. "Come on," I said. "Do as I say." I flung the
corridor door open.

Getting Bertha Cool out of that room was like pulling a bristling
dog away from another dog who's trying to pick a fight. Bertha Cool
finally came out into the corridor, but she didn't want to come.
She was mad clean through. She didn't like the way I was playing
the game, and she wanted to stay and have it out with Flo Danzer.

Flo Danzer didn't say anything. She'd got control of her face now,
and it was set in an expression of tightlipped hostility.

Out in the hallway, Bertha said, "My God, Donald, what's the matter
with you? You've called the turn on her, and she's just about ready
to cave."

I said, "No, she isn't. You two women will start fighting. We
haven't enough cards to call for a showdown."

"Why haven't we?"

"Because we can't prove anything. All we can do is bluff. Remember,
the object of this visit was to make her call Harbet. She'll call
him now. What she'll say over the telephone will make that
switchboard operator's hair stand right up on end. She'll be
listening in on that conversation. By the time we know what's said
over the telephone, we'll be ready to call for a showdown. Then
we'll have some proof. Now, we're just running a bluff."

We went down in the elevator. I paused at the switchboard to say,
"Thank you very much," and added in a lower voice, "I'll ring you
in fifteen minutes."

Bertha Cool paused at the clerk's desk to flash her diamonds. "You
have very lovely apartments," she said, with that gracious smile of
hers, and the clerk came out from behind the shell of icy reserve
to smile all over his face. "In case you're interested," he said,
"we have one or two choice vacancies."

"Perhaps a little later," Bertha Cool said, nodding with just the
right amount of condescension, and sailing majestically out of the
door, which I deferentially held open. She looked for all the world
like Mrs. Million-bucks taking her pet diamonds out for an airing.

I indicated the agency car. Bertha Cool said, "To hell with that
bunch of junk. He may be looking out of the door. We'll get a
taxi."

"We won't find one cruising along here," I said.

"We'll stop at a drugstore and telephone."

I said, "Let's go up and see Marian," and then watched Bertha
Cool's face out of the corner of my eye.

She said, "No, lover, we can't go see Marian."

"Why not?"

"I'll explain to you later. You haven't seen the morning papers."

I said, "No, I've been on the job all night."

"I know, Donald. Now listen, we can't go to the office. We can't go
to your place. We can't go to Marian's place. I'll telephone for a
taxi. You go back and tell the relief operators to telephone
reports in to me at the West-mount Hotel. We'll go there.' I said,
“What's in the morning papers? I'd better buy one."

"Not now, lover," she said. "Just keep your mind on this."

I said, "All right. You get the cab and pick me up."

I walked back to the operatives on duty and told them to report to
Bertha at the Westmount Hotel, and, in case there was no answer
there, to ring the agency and report to Miss Brand.

I was half-way back to the drugstore when Bertha showed up with a
taxi. I climbed in, and we drove to the Westmount Hotel in silence.
Bertha had a morning paper clamped under her arm, but she wouldn't
let me see it.



